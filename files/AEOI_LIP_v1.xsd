<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="urn:ec.europa.eu:taxud:directtax:aeoi:v1" xmlns:aeoi="urn:ec.europa.eu:taxud:directtax:aeoi:v1" xmlns:cm="urn:ec.europa.eu:taxud:directtax:aeoi:common:v1" xmlns:dt="urn:cec.eu.int:taxud:directtax:v3" xmlns:stfm="urn:cec.eu.int:taxud:directtax:eusd:v3" targetNamespace="urn:ec.europa.eu:taxud:directtax:aeoi:v1" elementFormDefault="qualified" attributeFormDefault="unqualified"  version="1.01">
	<!--HISTORY	
		22/04/2013 - Initial version.
		13/03/2017 - Integrated directtax_v3.xsd version 3.05 regarding the territorial features.
	-->
	
	<xs:import namespace="urn:ec.europa.eu:taxud:directtax:aeoi:common:v1" schemaLocation="commontypes_v1.xsd"/>
	<xs:import namespace="urn:cec.eu.int:taxud:directtax:v3" schemaLocation="directtax_v3.xsd"/>
	<xs:import namespace="urn:cec.eu.int:taxud:directtax:eusd:v3" schemaLocation="stfmodifiedtypes_v3.xsd"/>
	
	<!--
		Data Types
	-->
	
	<xs:simpleType name="LIPActorType_Type">
		<xs:restriction base="xs:token">
			<xs:enumeration value="AbsoluteEntitlement"/>
			<xs:enumeration value="OnBehalf"/>
			<xs:enumeration value="UltimateBeneficialOwner"/>
			<xs:enumeration value="Unknown"/>
		</xs:restriction>
	</xs:simpleType>
	
	<xs:simpleType name="LIPBeneficiaryStatus_Type">
		<xs:restriction base="xs:token">
			<xs:enumeration value="Revocable"/>
			<xs:enumeration value="Irrevocable"/>
		</xs:restriction>
	</xs:simpleType>
	
	<xs:simpleType name="LIPBenefitType_Type">
		<xs:restriction base="xs:token">
			<xs:enumeration value="AnnuityWholeLife"/>
			<xs:enumeration value="AnnuityFixedTerm"/>
			<xs:enumeration value="OtherSpread"/>
			<xs:enumeration value="LumpSum"/>
			<xs:enumeration value="Unknown"/>
		</xs:restriction>
	</xs:simpleType>
	
	<xs:simpleType name="LIPContributionType_Type">
		<xs:restriction base="xs:token">
			<xs:enumeration value="FixedTerm"/>
			<xs:enumeration value="WholeLife"/>
		</xs:restriction>
	</xs:simpleType>
	
	<xs:simpleType name="LIPContributionFrequency_Type">
		<xs:restriction base="xs:token">
			<xs:enumeration value="SinglePremium"/>
			<xs:enumeration value="RegularPremium"/>
		</xs:restriction>
	</xs:simpleType>
	
	<xs:simpleType name="LIPEventScope_Type">
		<xs:restriction base="xs:token">
			<xs:enumeration value="Full"/>
			<xs:enumeration value="Partial"/>
		</xs:restriction>
	</xs:simpleType>
	
	<xs:simpleType name="LIPEventType_Type">
		<xs:restriction base="xs:token">
			<xs:enumeration value="Contribution"/>
			<xs:enumeration value="OtherUnspecifiedIn"/>
			<xs:enumeration value="Annuity"/>
			<xs:enumeration value="LumpSumAtMaturity"/>
			<xs:enumeration value="Death"/>
			<xs:enumeration value="Disability"/>
			<xs:enumeration value="Illness"/>
			<xs:enumeration value="SurrenderOfRightsRepayment"/>
			<xs:enumeration value="OtherUnspecifiedOut"/>
			<xs:enumeration value="TransferInScheme"/>
			<xs:enumeration value="TransferInNational"/>
			<xs:enumeration value="TransferInInternational"/>
			<xs:enumeration value="TransferInOtherUnspecified"/>
			<xs:enumeration value="TransferOutScheme"/>
			<xs:enumeration value="TransferOutNational"/>
			<xs:enumeration value="TransferOutInternational"/>
			<xs:enumeration value="TransferOutOtherUnspecified"/>
			<xs:enumeration value="CollateralPledge"/>
			<xs:enumeration value="AdvanceIn"/>
			<xs:enumeration value="AdvanceOut"/>
			<xs:enumeration value="RefundOfPremiums"/>
			<xs:enumeration value="AcceptanceByTheBeneficiary"/>
			<xs:enumeration value="OtherOtherUnspecified"/>
			<xs:enumeration value="Yield"/>
			<xs:enumeration value="OtherYield"/>
		</xs:restriction>
	</xs:simpleType> 
	
	<xs:simpleType name="LIPPolicyCapitalValueType_Type">
		<xs:restriction base="xs:token">
			<xs:enumeration value="TechnicalSurrenderValue"/>
			<xs:enumeration value="MarketValue"/>
			<xs:enumeration value="Other"/>
			<xs:enumeration value="Unknown"/>
		</xs:restriction>
	</xs:simpleType>
	
	<xs:simpleType name="LIPPolicyType_Type">
		<xs:restriction base="xs:token">
			<xs:enumeration value="PureRisk"/>
			<xs:enumeration value="Other"/>
		</xs:restriction>
	</xs:simpleType>
	
	<xs:simpleType name="LIPRelationshipToBeneficiary_Type">
		<xs:restriction base="xs:token">
			<xs:enumeration value="Employer"/>
			<xs:enumeration value="SpouseOrPartner"/>
			<xs:enumeration value="ParentOrChildren"/>
			<xs:enumeration value="OwnBusinessOrConnectedEntity"/>
		</xs:restriction>
	</xs:simpleType>
	
	<xs:simpleType name="LIPStatus_Type">
		<xs:restriction base="xs:token">
			<xs:enumeration value="TotalPremiumPaid"/>
			<xs:enumeration value="NetIncomeProfit"/>
			<xs:enumeration value="GrossEventProceeds"/>
		</xs:restriction>
	</xs:simpleType>
	
	<xs:simpleType name="LIPTax_Type">
		<xs:restriction base="xs:token">
			<xs:enumeration value="TaxReliefOrDeduction"/>
			<xs:enumeration value="PaymentIn"/>
			<xs:enumeration value="PaymentOut"/>
			<xs:enumeration value="TransferIn"/>
			<xs:enumeration value="TransferOut"/>
			<xs:enumeration value="YieldTaxCapitalTax"/>
			<xs:enumeration value="OtherUnspecified"/>
		</xs:restriction>
	</xs:simpleType>
	
	<xs:simpleType name="LIPTaxationTypeEvent_Type">
		<xs:restriction base="xs:token">
			<xs:enumeration value="TaxableUponReceipt"/>
			<xs:enumeration value="TaxationSpread"/>
			<xs:enumeration value="Exempt"/>
			<xs:enumeration value="ExemptNational"/>
			<xs:enumeration value="ExemptTreaty"/>
			<xs:enumeration value="Unknown"/>
		</xs:restriction>
	</xs:simpleType>
	
	<xs:simpleType name="LIPTaxationTypeTax_Type">
		<xs:restriction base="xs:token">
			<xs:enumeration value="NoWithholding"/>
			<xs:enumeration value="SelfInstalment"/>
			<xs:enumeration value="Withholding"/>
			<xs:enumeration value="Final"/>
			<xs:enumeration value="Unknown"/>
		</xs:restriction>
	</xs:simpleType>
	
	<xs:simpleType name="LIPTransferType_Type">
		<xs:restriction base="xs:token">
			<xs:enumeration value="ByGift"/>
			<xs:enumeration value="SalePurchase"/>
			<xs:enumeration value="Inheritance"/>
			<xs:enumeration value="Other"/>
			<xs:enumeration value="Unknown"/>
		</xs:restriction>
	</xs:simpleType>
	
	<!--
		Message
	-->
	
	<xs:simpleType name="LIPApplicationId_Type">
		<xs:restriction base="xs:token">
			<xs:enumeration value="AEOI-LIP"/>
		</xs:restriction>
	</xs:simpleType>
	
	<xs:complexType name="AEOILIPBody_Type">
		<xs:annotation><xs:documentation xml:lang="en">Concrete implementation of the abstract body for LIP</xs:documentation></xs:annotation>
		<xs:complexContent>
			<xs:restriction base="cm:AEOIBodyWithTaxYear_Type">
				<xs:sequence>
					<xs:choice>
						<xs:sequence>
							<xs:element name="Policies" type="AEOILIPRecord_Type">
								<xs:unique name="LIPPolicyDocRefId_Key">
									<xs:selector xpath="aeoi:Policy/aeoi:DocSpec"/>
									<xs:field xpath="stfm:DocRefId"/>
								</xs:unique>
							</xs:element>
							<xs:element name="PolicyInvalidations" type="AEOILIPRecordInvalidation_Type" minOccurs="0">
								<xs:unique name="LIPPolicyInvalidationDocRefId_Key">
									<xs:selector xpath="aeoi:PolicyInvalidation"/>
									<xs:field xpath="stfm:DocRefId"/>
								</xs:unique>
							</xs:element>
						</xs:sequence>
						<xs:element name="PolicyInvalidations" type="AEOILIPRecordInvalidation_Type">
							<xs:unique name="LIPPolicyInvalidation2DocRefId_Key">
								<xs:selector xpath="aeoi:PolicyInvalidation"/>
								<xs:field xpath="stfm:DocRefId"/>
							</xs:unique>
						</xs:element> 
					</xs:choice>
				</xs:sequence>
				<xs:attribute name="applicationId" use="required" type="LIPApplicationId_Type"/>
			</xs:restriction>
		</xs:complexContent>
	</xs:complexType>
	
	<xs:complexType name="AEOILIPRecord_Type">
		<xs:sequence>
			<xs:element name="Policy" type="LIPPolicy_Type" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	
	<xs:complexType name="AEOILIPRecordInvalidation_Type">
		<xs:sequence>
			<xs:element name="PolicyInvalidation" type="stfm:DocSpec_Type" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	
	<xs:complexType name="LIPPolicy_Type">
		<xs:sequence>
			<xs:element name="ContributionDuration" type="LIPContributionDuration_Type" minOccurs="0"/>
			<xs:element name="BenefitDuration" type="LIPBenefitDuration_Type" minOccurs="0"/>
			<xs:element name="PolicyOptions" type="LIPPolicyOptions_Type" minOccurs="0"/>
			<xs:element name="PolicyCapitalValues" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="PolicyCapitalValue" type="LIPPolicyCapitalValue_Type" maxOccurs="unbounded"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="InsurerOrPayingAgent" type="LIPInsurerOrPayingAgent_Type"/>
			<xs:element name="Beneficiaries" minOccurs="0">
				<xs:complexType>
					<xs:choice maxOccurs="unbounded">
						<xs:element name="NamedBeneficiary" type="LIPNamedBeneficiary_Type"/>
						<xs:element name="ClassBeneficiary" type="LIPClassBeneficiary_Type"/>
					</xs:choice>
				</xs:complexType>
			</xs:element>
			<xs:element name="LifeInsured" minOccurs="0">
				<xs:complexType>
					<xs:choice maxOccurs="unbounded">
						<xs:element name="NamedLifeInsured" type="LIPNamedLifeInsured_Type"/>
						<xs:element name="ClassLifeInsured" type="LIPClassLifeInsured_Type"/>
					</xs:choice>
				</xs:complexType>
			</xs:element>
			<xs:element name="PayersOfPremiums" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="PayerOfPremiums" type="LIPPayerOfPremiums_Type" maxOccurs="unbounded"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="Owners" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="Owner" type="LIPPolicyOwner_Type" maxOccurs="unbounded"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="PolicyNumber" type="xs:token" minOccurs="0"/>
			<xs:element name="EUSDIndicator" type="xs:boolean" minOccurs="0"/>
			<xs:element name="ContributionTaxTreatment" type="cm:ContributionTaxTreatment_Type"  minOccurs="0"/>
			<xs:element name="ContributionFrequency" type="LIPContributionFrequency_Type"  minOccurs="0"/>
			<xs:element name="StartDate" type="cm:AdjustablePrecisionDate_Type"  minOccurs="0"/>
			<xs:element name="EndDate" type="cm:AdjustablePrecisionDate_Type"  minOccurs="0"/>
			<xs:element name="ProfitSharing" type="xs:boolean"  minOccurs="0"/>
			<xs:element name="OtherInfo" type="cm:I18nString_Type"  minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="Events" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="Event" type="LIPEvent_Type" maxOccurs="unbounded"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="DocSpec" type="stfm:DocSpec_Type"/>
		</xs:sequence>
		<xs:attribute name="policyType" type="LIPPolicyType_Type"/>
	</xs:complexType>
	
	<xs:complexType name="LIPContributionDuration_Type">
		<xs:sequence>
			<xs:element name="ContributionMinimumPayableYears" type="cm:LimitedInteger_Type" minOccurs="0"/>
		</xs:sequence>
		<xs:attribute name="contributionDurationType" type="LIPContributionType_Type" use="required"/>
	</xs:complexType>
	
	<xs:complexType name="LIPBenefitDuration_Type">
		<xs:sequence>
			<xs:element name="BenefitMinimumPayableYears" type="cm:LimitedInteger_Type" minOccurs="0"/>
		</xs:sequence>
		<xs:attribute name="benefitType" type="LIPBenefitType_Type" use="required"/>
	</xs:complexType>
	
	<xs:complexType name="LIPPolicyOptions_Type">
		<xs:sequence>
			<xs:element name="EarlyRedemptionOption" type="xs:boolean" minOccurs="0"/>
			<xs:element name="CollateralOption" type="xs:boolean" minOccurs="0"/>
			<xs:element name="TransferOption" type="xs:boolean" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	
	<xs:complexType name="LIPPolicyCapitalValue_Type">
		<xs:sequence>
			<xs:element name="Amount" type="cm:MoneyAmount_Type"/>
			<xs:element name="Date" type="cm:AdjustablePrecisionDate_Type"/>
		</xs:sequence>
		<xs:attribute name="policyCapitalValueType" type="LIPPolicyCapitalValueType_Type" use="required"/>
	</xs:complexType>
	
	<xs:complexType name="LIPEvent_Type">
		<xs:sequence>
			<xs:element name="ActualDate" type="cm:AdjustablePrecisionDate_Type" minOccurs="0"/>
			<xs:element name="DeemedDate" type="cm:AdjustablePrecisionDate_Type" minOccurs="0"/>
			<xs:element name="Scope" type="LIPEventScope_Type" minOccurs="0"/>
			<xs:element name="TransferType" type="LIPTransferType_Type" minOccurs="0"/>
			<xs:element name="Periodicity" type="cm:Periodicity_Type"/>
			<xs:choice minOccurs="0">
				<xs:element name="NaturalCounterpart" type="cm:NaturalPersonOpt_Type"/>
				<xs:element name="NonNaturalCounterpart" type="cm:NonNaturalPersonOpt_Type"/>
			</xs:choice>
			<xs:choice><!-- One or the other or both -->
				<xs:element name="EventInfo" type="LIPEventInfo_Type" maxOccurs="unbounded"/>
				<xs:sequence>
					<xs:element name="TaxInfo" type="LIPTaxInfo_Type" maxOccurs="unbounded"/>
					<xs:element name="EventInfo" type="LIPEventInfo_Type" minOccurs="0" maxOccurs="unbounded"/>
				</xs:sequence>
			</xs:choice>
		</xs:sequence>
		<xs:attribute name="eventType" type="LIPEventType_Type" use="required"/>
	</xs:complexType>
	
	<xs:complexType name="LIPEventInfo_Type">
		<xs:sequence>
			<xs:choice>
				<xs:element name="Existence" type="cm:Existence_Type"/>
				<xs:element name="FinancialInfo" type="cm:FinancialInfo_Type"/>
			</xs:choice>
			<xs:element name="Status" type="LIPStatus_Type" minOccurs="0"/>
			<xs:element name="TaxationTypeEvent" type="LIPTaxationTypeEvent_Type"/>
			<xs:element name="EventBasisType" type="cm:TaxableBasisType_Type" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	
	<xs:complexType name="LIPTaxInfo_Type">
		<xs:sequence>
			<xs:element name="Tax" type="LIPTax_Type"/>
			<xs:choice>
				<xs:element name="Existence" type="cm:Existence_Type"/>
				<xs:element name="FinancialInfo" type="cm:FinancialInfo_Type"/>
			</xs:choice>
			<xs:element name="TaxationTypeTax" type="LIPTaxationTypeTax_Type"/>
			<xs:element name="TaxableBasisType" type="cm:TaxableBasisType_Type" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	
	<xs:complexType name="LIPInsurerOrPayingAgent_Type">
		<xs:sequence>
			<xs:choice>
				<xs:element name="NaturalPerson" type="cm:NaturalPerson_Type"/>
				<xs:element name="NonNaturalPerson" type="cm:NonNaturalPerson_Type"/>
			</xs:choice>
			<xs:choice minOccurs="0">
				<xs:element name="NaturalRepresentative" type="cm:NaturalPersonOpt_Type"/>
				<xs:element name="NonNaturalRepresentative" type="cm:NonNaturalPersonOpt_Type"/>
			</xs:choice>
		</xs:sequence>
	</xs:complexType>
	
	<xs:complexType name="LIPNamedBeneficiary_Type">
		<xs:sequence>
			<xs:choice>
				<xs:element name="NaturalPerson" type="cm:NaturalPerson_Type"/>
				<xs:element name="NonNaturalPerson" type="cm:NonNaturalPerson_Type"/>
			</xs:choice>
			<xs:choice minOccurs="0">
				<xs:element name="NaturalRepresentative" type="cm:NaturalPersonOpt_Type"/>
				<xs:element name="NonNaturalRepresentative" type="cm:NonNaturalPersonOpt_Type"/>
			</xs:choice>
			<xs:element name="ShareType" type="cm:ShareType_Type"/>
			<xs:element name="SharePart" type="cm:Percentage_Type" minOccurs="0"/>
			<xs:element name="Status" type="LIPBeneficiaryStatus_Type" minOccurs="0"/>
		</xs:sequence>
		<xs:attribute name="beneficiaryType" type="LIPActorType_Type" use="required"/>
	</xs:complexType>
	
	<xs:complexType name="LIPClassBeneficiary_Type">
		<xs:sequence>
			<xs:element name="ClassIdentification" type="LIPRelationshipToBeneficiary_Type"/>
			<xs:choice minOccurs="0">
				<xs:element name="NaturalRepresentative" type="cm:NaturalPersonOpt_Type"/>
				<xs:element name="NonNaturalRepresentative" type="cm:NonNaturalPersonOpt_Type"/>
			</xs:choice>
		</xs:sequence>
	</xs:complexType>
	
	<xs:complexType name="LIPNamedLifeInsured_Type">
		<xs:sequence>
			<xs:element name="NaturalPerson" type="cm:NaturalPerson_Type"/>
			<xs:choice minOccurs="0">
				<xs:element name="NaturalRepresentative" type="cm:NaturalPersonOpt_Type"/>
				<xs:element name="NonNaturalRepresentative" type="cm:NonNaturalPersonOpt_Type"/>
			</xs:choice>
			<xs:element name="RelationshipToBeneficiary" type="LIPRelationshipToBeneficiary_Type" minOccurs="0"/>
			<xs:element name="ShareType" type="cm:ShareType_Type"/>
			<xs:element name="SharePart" type="cm:Percentage_Type" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	
	<xs:complexType name="LIPClassLifeInsured_Type">
		<xs:sequence>
			<xs:element name="ClassIdentification" type="LIPRelationshipToBeneficiary_Type"/>
			<xs:choice minOccurs="0">
				<xs:element name="NaturalRepresentative" type="cm:NaturalPersonOpt_Type"/>
				<xs:element name="NonNaturalRepresentative" type="cm:NonNaturalPersonOpt_Type"/>
			</xs:choice>
		</xs:sequence>
	</xs:complexType>
	
	<xs:complexType name="LIPPolicyOwner_Type">
		<xs:sequence>
			<xs:choice>
				<xs:element name="NaturalPerson" type="cm:NaturalPerson_Type"/>
				<xs:element name="NonNaturalPerson" type="cm:NonNaturalPerson_Type"/>
			</xs:choice>
			<xs:choice minOccurs="0">
				<xs:element name="NaturalRepresentative" type="cm:NaturalPersonOpt_Type"/>
				<xs:element name="NonNaturalRepresentative" type="cm:NonNaturalPersonOpt_Type"/>
			</xs:choice>
			<xs:element name="RelationshipToBeneficiary" type="LIPRelationshipToBeneficiary_Type" minOccurs="0"/>
			<xs:element name="ShareType" type="cm:ShareType_Type"/>
			<xs:element name="SharePart" type="cm:Percentage_Type" minOccurs="0"/>
		</xs:sequence>
		<xs:attribute name="ownerType" type="LIPActorType_Type" use="required"/>
	</xs:complexType>
	
	<xs:complexType name="LIPPayerOfPremiums_Type">
		<xs:sequence>
			<xs:choice>
				<xs:element name="NaturalPerson" type="cm:NaturalPerson_Type"/>
				<xs:element name="NonNaturalPerson" type="cm:NonNaturalPerson_Type"/>
			</xs:choice>
			<xs:choice minOccurs="0">
				<xs:element name="NaturalRepresentative" type="cm:NaturalPersonOpt_Type"/>
				<xs:element name="NonNaturalRepresentative" type="cm:NonNaturalPersonOpt_Type"/>
			</xs:choice>
			<xs:element name="RelationshipToBeneficiary" type="LIPRelationshipToBeneficiary_Type" minOccurs="0"/>
			<xs:element name="ShareType" type="cm:ShareType_Type"/>
			<xs:element name="SharePart" type="cm:Percentage_Type" minOccurs="0"/>
		</xs:sequence>
		<xs:attribute name="payerType" type="LIPActorType_Type" use="required"/>
	</xs:complexType>
</xs:schema>
