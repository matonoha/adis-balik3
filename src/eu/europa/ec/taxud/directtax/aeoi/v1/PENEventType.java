//
// This file was generated by the Eclipse Implementation of JAXB, v2.3.3 
// See https://eclipse-ee4j.github.io/jaxb-ri 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.21 at 10:14:16 AM CEST 
//


package eu.europa.ec.taxud.directtax.aeoi.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.AdjustablePrecisionDateType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.NaturalPersonOptType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.NonNaturalPersonOptType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.ReportingTypeType;


/**
 * &lt;p&gt;Java class for PENEvent_Type complex type.
 * 
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="PENEvent_Type"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="StartDate" type="{urn:ec.europa.eu:taxud:directtax:aeoi:common:v1}AdjustablePrecisionDate_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="EndDate" type="{urn:ec.europa.eu:taxud:directtax:aeoi:common:v1}AdjustablePrecisionDate_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Periodicity" type="{urn:ec.europa.eu:taxud:directtax:aeoi:common:v1}Periodicity_Type"/&amp;gt;
 *         &amp;lt;element name="ReportingType" type="{urn:ec.europa.eu:taxud:directtax:aeoi:common:v1}ReportingType_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="TransferType" type="{urn:ec.europa.eu:taxud:directtax:aeoi:v1}PENTransferType_Type" minOccurs="0"/&amp;gt;
 *         &amp;lt;choice minOccurs="0"&amp;gt;
 *           &amp;lt;element name="NaturalCounterpart" type="{urn:ec.europa.eu:taxud:directtax:aeoi:common:v1}NaturalPersonOpt_Type"/&amp;gt;
 *           &amp;lt;element name="NonNaturalCounterpart" type="{urn:ec.europa.eu:taxud:directtax:aeoi:common:v1}NonNaturalPersonOpt_Type"/&amp;gt;
 *         &amp;lt;/choice&amp;gt;
 *         &amp;lt;choice&amp;gt;
 *           &amp;lt;element name="EventInfo" type="{urn:ec.europa.eu:taxud:directtax:aeoi:v1}PENEventInfo_Type" maxOccurs="unbounded"/&amp;gt;
 *           &amp;lt;sequence&amp;gt;
 *             &amp;lt;element name="TaxInfo" type="{urn:ec.europa.eu:taxud:directtax:aeoi:v1}PENTaxInfo_Type" maxOccurs="unbounded"/&amp;gt;
 *             &amp;lt;element name="EventInfo" type="{urn:ec.europa.eu:taxud:directtax:aeoi:v1}PENEventInfo_Type" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *           &amp;lt;/sequence&amp;gt;
 *         &amp;lt;/choice&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="eventType" use="required" type="{urn:ec.europa.eu:taxud:directtax:aeoi:v1}PENEventType_Type" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PENEvent_Type", propOrder = {
    "content"
})
public class PENEventType {

    @XmlElementRefs({
        @XmlElementRef(name = "StartDate", namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "EndDate", namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Periodicity", namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "ReportingType", namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "TransferType", namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "NaturalCounterpart", namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "NonNaturalCounterpart", namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "EventInfo", namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "TaxInfo", namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", type = JAXBElement.class, required = false)
    })
    protected List<JAXBElement<?>> content;
    @XmlAttribute(name = "eventType", required = true)
    protected PENEventTypeType eventType;

    /**
     * Gets the rest of the content model. 
     * 
     * &lt;p&gt;
     * You are getting this "catch-all" property because of the following reason: 
     * The field name "EventInfo" is used by two different parts of a schema. See: 
     * line 384 of file:/C:/Users/Matonoha/balik-prace-3/adis.balik3/files/AEOI_PEN_v1.xsd
     * line 381 of file:/C:/Users/Matonoha/balik-prace-3/adis.balik3/files/AEOI_PEN_v1.xsd
     * &lt;p&gt;
     * To get rid of this property, apply a property customization to one 
     * of both of the following declarations to change their names: 
     * Gets the value of the content property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the content property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getContent().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link AdjustablePrecisionDateType }{@code >}
     * {@link JAXBElement }{@code <}{@link AdjustablePrecisionDateType }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link ReportingTypeType }{@code >}
     * {@link JAXBElement }{@code <}{@link PENTransferTypeType }{@code >}
     * {@link JAXBElement }{@code <}{@link NaturalPersonOptType }{@code >}
     * {@link JAXBElement }{@code <}{@link NonNaturalPersonOptType }{@code >}
     * {@link JAXBElement }{@code <}{@link PENEventInfoType }{@code >}
     * {@link JAXBElement }{@code <}{@link PENTaxInfoType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<?>> getContent() {
        if (content == null) {
            content = new ArrayList<JAXBElement<?>>();
        }
        return this.content;
    }

    /**
     * Gets the value of the eventType property.
     * 
     * @return
     *     possible object is
     *     {@link PENEventTypeType }
     *     
     */
    public PENEventTypeType getEventType() {
        return eventType;
    }

    /**
     * Sets the value of the eventType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PENEventTypeType }
     *     
     */
    public void setEventType(PENEventTypeType value) {
        this.eventType = value;
    }

}
