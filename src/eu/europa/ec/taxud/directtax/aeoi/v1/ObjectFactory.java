//
// This file was generated by the Eclipse Implementation of JAXB, v2.3.3 
// See https://eclipse-ee4j.github.io/jaxb-ri 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.21 at 10:14:16 AM CEST 
//


package eu.europa.ec.taxud.directtax.aeoi.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.namespace.QName;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.AdjustablePrecisionDateType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.NaturalPersonOptType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.NonNaturalPersonOptType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.ReportingTypeType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the eu.europa.ec.taxud.directtax.aeoi.v1 package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _LIPEventTypeActualDate_QNAME = new QName("urn:ec.europa.eu:taxud:directtax:aeoi:v1", "ActualDate");
    private final static QName _LIPEventTypeDeemedDate_QNAME = new QName("urn:ec.europa.eu:taxud:directtax:aeoi:v1", "DeemedDate");
    private final static QName _LIPEventTypeScope_QNAME = new QName("urn:ec.europa.eu:taxud:directtax:aeoi:v1", "Scope");
    private final static QName _LIPEventTypeTransferType_QNAME = new QName("urn:ec.europa.eu:taxud:directtax:aeoi:v1", "TransferType");
    private final static QName _LIPEventTypePeriodicity_QNAME = new QName("urn:ec.europa.eu:taxud:directtax:aeoi:v1", "Periodicity");
    private final static QName _LIPEventTypeNaturalCounterpart_QNAME = new QName("urn:ec.europa.eu:taxud:directtax:aeoi:v1", "NaturalCounterpart");
    private final static QName _LIPEventTypeNonNaturalCounterpart_QNAME = new QName("urn:ec.europa.eu:taxud:directtax:aeoi:v1", "NonNaturalCounterpart");
    private final static QName _LIPEventTypeEventInfo_QNAME = new QName("urn:ec.europa.eu:taxud:directtax:aeoi:v1", "EventInfo");
    private final static QName _LIPEventTypeTaxInfo_QNAME = new QName("urn:ec.europa.eu:taxud:directtax:aeoi:v1", "TaxInfo");
    private final static QName _PENEventTypeStartDate_QNAME = new QName("urn:ec.europa.eu:taxud:directtax:aeoi:v1", "StartDate");
    private final static QName _PENEventTypeEndDate_QNAME = new QName("urn:ec.europa.eu:taxud:directtax:aeoi:v1", "EndDate");
    private final static QName _PENEventTypeReportingType_QNAME = new QName("urn:ec.europa.eu:taxud:directtax:aeoi:v1", "ReportingType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eu.europa.ec.taxud.directtax.aeoi.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LIPPolicyType }
     * 
     */
    public LIPPolicyType createLIPPolicyType() {
        return new LIPPolicyType();
    }

    /**
     * Create an instance of {@link PENSchemeType }
     * 
     */
    public PENSchemeType createPENSchemeType() {
        return new PENSchemeType();
    }

    /**
     * Create an instance of {@link PENPayerType }
     * 
     */
    public PENPayerType createPENPayerType() {
        return new PENPayerType();
    }

    /**
     * Create an instance of {@link PENRecipientType }
     * 
     */
    public PENRecipientType createPENRecipientType() {
        return new PENRecipientType();
    }

    /**
     * Create an instance of {@link IPLoanType }
     * 
     */
    public IPLoanType createIPLoanType() {
        return new IPLoanType();
    }

    /**
     * Create an instance of {@link IPRightType }
     * 
     */
    public IPRightType createIPRightType() {
        return new IPRightType();
    }

    /**
     * Create an instance of {@link IPPropertyType }
     * 
     */
    public IPPropertyType createIPPropertyType() {
        return new IPPropertyType();
    }

    /**
     * Create an instance of {@link IPPartyType }
     * 
     */
    public IPPartyType createIPPartyType() {
        return new IPPartyType();
    }

    /**
     * Create an instance of {@link IERelationshipType }
     * 
     */
    public IERelationshipType createIERelationshipType() {
        return new IERelationshipType();
    }

    /**
     * Create an instance of {@link IEPayerType }
     * 
     */
    public IEPayerType createIEPayerType() {
        return new IEPayerType();
    }

    /**
     * Create an instance of {@link IERecipientType }
     * 
     */
    public IERecipientType createIERecipientType() {
        return new IERecipientType();
    }

    /**
     * Create an instance of {@link AEOIIEBodyType }
     * 
     */
    public AEOIIEBodyType createAEOIIEBodyType() {
        return new AEOIIEBodyType();
    }

    /**
     * Create an instance of {@link AEOIIERecordType }
     * 
     */
    public AEOIIERecordType createAEOIIERecordType() {
        return new AEOIIERecordType();
    }

    /**
     * Create an instance of {@link AEOIIERecordInvalidationType }
     * 
     */
    public AEOIIERecordInvalidationType createAEOIIERecordInvalidationType() {
        return new AEOIIERecordInvalidationType();
    }

    /**
     * Create an instance of {@link IEWorkplaceType }
     * 
     */
    public IEWorkplaceType createIEWorkplaceType() {
        return new IEWorkplaceType();
    }

    /**
     * Create an instance of {@link IEIncomeType }
     * 
     */
    public IEIncomeType createIEIncomeType() {
        return new IEIncomeType();
    }

    /**
     * Create an instance of {@link IEQuantityType }
     * 
     */
    public IEQuantityType createIEQuantityType() {
        return new IEQuantityType();
    }

    /**
     * Create an instance of {@link AEOIIPBodyType }
     * 
     */
    public AEOIIPBodyType createAEOIIPBodyType() {
        return new AEOIIPBodyType();
    }

    /**
     * Create an instance of {@link AEOIIPRecordType }
     * 
     */
    public AEOIIPRecordType createAEOIIPRecordType() {
        return new AEOIIPRecordType();
    }

    /**
     * Create an instance of {@link AEOIIPRecordInvalidationType }
     * 
     */
    public AEOIIPRecordInvalidationType createAEOIIPRecordInvalidationType() {
        return new AEOIIPRecordInvalidationType();
    }

    /**
     * Create an instance of {@link IPIncomeType }
     * 
     */
    public IPIncomeType createIPIncomeType() {
        return new IPIncomeType();
    }

    /**
     * Create an instance of {@link IPIncomeInformationType }
     * 
     */
    public IPIncomeInformationType createIPIncomeInformationType() {
        return new IPIncomeInformationType();
    }

    /**
     * Create an instance of {@link IPSizeInfoType }
     * 
     */
    public IPSizeInfoType createIPSizeInfoType() {
        return new IPSizeInfoType();
    }

    /**
     * Create an instance of {@link IPValueInfoType }
     * 
     */
    public IPValueInfoType createIPValueInfoType() {
        return new IPValueInfoType();
    }

    /**
     * Create an instance of {@link IPOwnershipType }
     * 
     */
    public IPOwnershipType createIPOwnershipType() {
        return new IPOwnershipType();
    }

    /**
     * Create an instance of {@link IPShareType }
     * 
     */
    public IPShareType createIPShareType() {
        return new IPShareType();
    }

    /**
     * Create an instance of {@link IPDatedCoefficientType }
     * 
     */
    public IPDatedCoefficientType createIPDatedCoefficientType() {
        return new IPDatedCoefficientType();
    }

    /**
     * Create an instance of {@link IPTransactionType }
     * 
     */
    public IPTransactionType createIPTransactionType() {
        return new IPTransactionType();
    }

    /**
     * Create an instance of {@link IPTransactionFinancialInfoType }
     * 
     */
    public IPTransactionFinancialInfoType createIPTransactionFinancialInfoType() {
        return new IPTransactionFinancialInfoType();
    }

    /**
     * Create an instance of {@link IPLoanCollateralType }
     * 
     */
    public IPLoanCollateralType createIPLoanCollateralType() {
        return new IPLoanCollateralType();
    }

    /**
     * Create an instance of {@link IPLoanInfoType }
     * 
     */
    public IPLoanInfoType createIPLoanInfoType() {
        return new IPLoanInfoType();
    }

    /**
     * Create an instance of {@link IPTimesharingType }
     * 
     */
    public IPTimesharingType createIPTimesharingType() {
        return new IPTimesharingType();
    }

    /**
     * Create an instance of {@link AEOIPENBodyType }
     * 
     */
    public AEOIPENBodyType createAEOIPENBodyType() {
        return new AEOIPENBodyType();
    }

    /**
     * Create an instance of {@link AEOIPENRecordType }
     * 
     */
    public AEOIPENRecordType createAEOIPENRecordType() {
        return new AEOIPENRecordType();
    }

    /**
     * Create an instance of {@link AEOIPENRecordInvalidationType }
     * 
     */
    public AEOIPENRecordInvalidationType createAEOIPENRecordInvalidationType() {
        return new AEOIPENRecordInvalidationType();
    }

    /**
     * Create an instance of {@link PENSchemeReferenceInfoType }
     * 
     */
    public PENSchemeReferenceInfoType createPENSchemeReferenceInfoType() {
        return new PENSchemeReferenceInfoType();
    }

    /**
     * Create an instance of {@link PENSchemeCapitalValueType }
     * 
     */
    public PENSchemeCapitalValueType createPENSchemeCapitalValueType() {
        return new PENSchemeCapitalValueType();
    }

    /**
     * Create an instance of {@link PENAdministratorType }
     * 
     */
    public PENAdministratorType createPENAdministratorType() {
        return new PENAdministratorType();
    }

    /**
     * Create an instance of {@link PENSchemeOwnerType }
     * 
     */
    public PENSchemeOwnerType createPENSchemeOwnerType() {
        return new PENSchemeOwnerType();
    }

    /**
     * Create an instance of {@link PENEventType }
     * 
     */
    public PENEventType createPENEventType() {
        return new PENEventType();
    }

    /**
     * Create an instance of {@link PENTaxInfoType }
     * 
     */
    public PENTaxInfoType createPENTaxInfoType() {
        return new PENTaxInfoType();
    }

    /**
     * Create an instance of {@link PENEventInfoType }
     * 
     */
    public PENEventInfoType createPENEventInfoType() {
        return new PENEventInfoType();
    }

    /**
     * Create an instance of {@link AEOILIPBodyType }
     * 
     */
    public AEOILIPBodyType createAEOILIPBodyType() {
        return new AEOILIPBodyType();
    }

    /**
     * Create an instance of {@link AEOILIPRecordType }
     * 
     */
    public AEOILIPRecordType createAEOILIPRecordType() {
        return new AEOILIPRecordType();
    }

    /**
     * Create an instance of {@link AEOILIPRecordInvalidationType }
     * 
     */
    public AEOILIPRecordInvalidationType createAEOILIPRecordInvalidationType() {
        return new AEOILIPRecordInvalidationType();
    }

    /**
     * Create an instance of {@link LIPContributionDurationType }
     * 
     */
    public LIPContributionDurationType createLIPContributionDurationType() {
        return new LIPContributionDurationType();
    }

    /**
     * Create an instance of {@link LIPBenefitDurationType }
     * 
     */
    public LIPBenefitDurationType createLIPBenefitDurationType() {
        return new LIPBenefitDurationType();
    }

    /**
     * Create an instance of {@link LIPPolicyOptionsType }
     * 
     */
    public LIPPolicyOptionsType createLIPPolicyOptionsType() {
        return new LIPPolicyOptionsType();
    }

    /**
     * Create an instance of {@link LIPPolicyCapitalValueType }
     * 
     */
    public LIPPolicyCapitalValueType createLIPPolicyCapitalValueType() {
        return new LIPPolicyCapitalValueType();
    }

    /**
     * Create an instance of {@link LIPEventType }
     * 
     */
    public LIPEventType createLIPEventType() {
        return new LIPEventType();
    }

    /**
     * Create an instance of {@link LIPEventInfoType }
     * 
     */
    public LIPEventInfoType createLIPEventInfoType() {
        return new LIPEventInfoType();
    }

    /**
     * Create an instance of {@link LIPTaxInfoType }
     * 
     */
    public LIPTaxInfoType createLIPTaxInfoType() {
        return new LIPTaxInfoType();
    }

    /**
     * Create an instance of {@link LIPInsurerOrPayingAgentType }
     * 
     */
    public LIPInsurerOrPayingAgentType createLIPInsurerOrPayingAgentType() {
        return new LIPInsurerOrPayingAgentType();
    }

    /**
     * Create an instance of {@link LIPNamedBeneficiaryType }
     * 
     */
    public LIPNamedBeneficiaryType createLIPNamedBeneficiaryType() {
        return new LIPNamedBeneficiaryType();
    }

    /**
     * Create an instance of {@link LIPClassBeneficiaryType }
     * 
     */
    public LIPClassBeneficiaryType createLIPClassBeneficiaryType() {
        return new LIPClassBeneficiaryType();
    }

    /**
     * Create an instance of {@link LIPNamedLifeInsuredType }
     * 
     */
    public LIPNamedLifeInsuredType createLIPNamedLifeInsuredType() {
        return new LIPNamedLifeInsuredType();
    }

    /**
     * Create an instance of {@link LIPClassLifeInsuredType }
     * 
     */
    public LIPClassLifeInsuredType createLIPClassLifeInsuredType() {
        return new LIPClassLifeInsuredType();
    }

    /**
     * Create an instance of {@link LIPPolicyOwnerType }
     * 
     */
    public LIPPolicyOwnerType createLIPPolicyOwnerType() {
        return new LIPPolicyOwnerType();
    }

    /**
     * Create an instance of {@link LIPPayerOfPremiumsType }
     * 
     */
    public LIPPayerOfPremiumsType createLIPPayerOfPremiumsType() {
        return new LIPPayerOfPremiumsType();
    }

    /**
     * Create an instance of {@link AEOISTBodyType }
     * 
     */
    public AEOISTBodyType createAEOISTBodyType() {
        return new AEOISTBodyType();
    }

    /**
     * Create an instance of {@link STErrorType }
     * 
     */
    public STErrorType createSTErrorType() {
        return new STErrorType();
    }

    /**
     * Create an instance of {@link AEOIZeroDataBodyType }
     * 
     */
    public AEOIZeroDataBodyType createAEOIZeroDataBodyType() {
        return new AEOIZeroDataBodyType();
    }

    /**
     * Create an instance of {@link ZDMainType }
     * 
     */
    public ZDMainType createZDMainType() {
        return new ZDMainType();
    }

    /**
     * Create an instance of {@link LIPPolicyType.PolicyCapitalValues }
     * 
     */
    public LIPPolicyType.PolicyCapitalValues createLIPPolicyTypePolicyCapitalValues() {
        return new LIPPolicyType.PolicyCapitalValues();
    }

    /**
     * Create an instance of {@link LIPPolicyType.Beneficiaries }
     * 
     */
    public LIPPolicyType.Beneficiaries createLIPPolicyTypeBeneficiaries() {
        return new LIPPolicyType.Beneficiaries();
    }

    /**
     * Create an instance of {@link LIPPolicyType.LifeInsured }
     * 
     */
    public LIPPolicyType.LifeInsured createLIPPolicyTypeLifeInsured() {
        return new LIPPolicyType.LifeInsured();
    }

    /**
     * Create an instance of {@link LIPPolicyType.PayersOfPremiums }
     * 
     */
    public LIPPolicyType.PayersOfPremiums createLIPPolicyTypePayersOfPremiums() {
        return new LIPPolicyType.PayersOfPremiums();
    }

    /**
     * Create an instance of {@link LIPPolicyType.Owners }
     * 
     */
    public LIPPolicyType.Owners createLIPPolicyTypeOwners() {
        return new LIPPolicyType.Owners();
    }

    /**
     * Create an instance of {@link LIPPolicyType.Events }
     * 
     */
    public LIPPolicyType.Events createLIPPolicyTypeEvents() {
        return new LIPPolicyType.Events();
    }

    /**
     * Create an instance of {@link PENSchemeType.SpecialActivityCategories }
     * 
     */
    public PENSchemeType.SpecialActivityCategories createPENSchemeTypeSpecialActivityCategories() {
        return new PENSchemeType.SpecialActivityCategories();
    }

    /**
     * Create an instance of {@link PENSchemeType.ReferenceInfos }
     * 
     */
    public PENSchemeType.ReferenceInfos createPENSchemeTypeReferenceInfos() {
        return new PENSchemeType.ReferenceInfos();
    }

    /**
     * Create an instance of {@link PENSchemeType.CapitalValues }
     * 
     */
    public PENSchemeType.CapitalValues createPENSchemeTypeCapitalValues() {
        return new PENSchemeType.CapitalValues();
    }

    /**
     * Create an instance of {@link PENSchemeType.Owners }
     * 
     */
    public PENSchemeType.Owners createPENSchemeTypeOwners() {
        return new PENSchemeType.Owners();
    }

    /**
     * Create an instance of {@link PENSchemeType.Events }
     * 
     */
    public PENSchemeType.Events createPENSchemeTypeEvents() {
        return new PENSchemeType.Events();
    }

    /**
     * Create an instance of {@link PENPayerType.Schemes }
     * 
     */
    public PENPayerType.Schemes createPENPayerTypeSchemes() {
        return new PENPayerType.Schemes();
    }

    /**
     * Create an instance of {@link PENRecipientType.Payers }
     * 
     */
    public PENRecipientType.Payers createPENRecipientTypePayers() {
        return new PENRecipientType.Payers();
    }

    /**
     * Create an instance of {@link IPLoanType.LoanCollaterals }
     * 
     */
    public IPLoanType.LoanCollaterals createIPLoanTypeLoanCollaterals() {
        return new IPLoanType.LoanCollaterals();
    }

    /**
     * Create an instance of {@link IPLoanType.LoanInfos }
     * 
     */
    public IPLoanType.LoanInfos createIPLoanTypeLoanInfos() {
        return new IPLoanType.LoanInfos();
    }

    /**
     * Create an instance of {@link IPRightType.Transactions }
     * 
     */
    public IPRightType.Transactions createIPRightTypeTransactions() {
        return new IPRightType.Transactions();
    }

    /**
     * Create an instance of {@link IPRightType.Incomes }
     * 
     */
    public IPRightType.Incomes createIPRightTypeIncomes() {
        return new IPRightType.Incomes();
    }

    /**
     * Create an instance of {@link IPRightType.Loans }
     * 
     */
    public IPRightType.Loans createIPRightTypeLoans() {
        return new IPRightType.Loans();
    }

    /**
     * Create an instance of {@link IPPropertyType.Ownerships }
     * 
     */
    public IPPropertyType.Ownerships createIPPropertyTypeOwnerships() {
        return new IPPropertyType.Ownerships();
    }

    /**
     * Create an instance of {@link IPPartyType.Incomes }
     * 
     */
    public IPPartyType.Incomes createIPPartyTypeIncomes() {
        return new IPPartyType.Incomes();
    }

    /**
     * Create an instance of {@link IPPartyType.Properties }
     * 
     */
    public IPPartyType.Properties createIPPartyTypeProperties() {
        return new IPPartyType.Properties();
    }

    /**
     * Create an instance of {@link IERelationshipType.Incomes }
     * 
     */
    public IERelationshipType.Incomes createIERelationshipTypeIncomes() {
        return new IERelationshipType.Incomes();
    }

    /**
     * Create an instance of {@link IEPayerType.Relationships }
     * 
     */
    public IEPayerType.Relationships createIEPayerTypeRelationships() {
        return new IEPayerType.Relationships();
    }

    /**
     * Create an instance of {@link IERecipientType.Payers }
     * 
     */
    public IERecipientType.Payers createIERecipientTypePayers() {
        return new IERecipientType.Payers();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdjustablePrecisionDateType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AdjustablePrecisionDateType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", name = "ActualDate", scope = LIPEventType.class)
    public JAXBElement<AdjustablePrecisionDateType> createLIPEventTypeActualDate(AdjustablePrecisionDateType value) {
        return new JAXBElement<AdjustablePrecisionDateType>(_LIPEventTypeActualDate_QNAME, AdjustablePrecisionDateType.class, LIPEventType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdjustablePrecisionDateType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AdjustablePrecisionDateType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", name = "DeemedDate", scope = LIPEventType.class)
    public JAXBElement<AdjustablePrecisionDateType> createLIPEventTypeDeemedDate(AdjustablePrecisionDateType value) {
        return new JAXBElement<AdjustablePrecisionDateType>(_LIPEventTypeDeemedDate_QNAME, AdjustablePrecisionDateType.class, LIPEventType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LIPEventScopeType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LIPEventScopeType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", name = "Scope", scope = LIPEventType.class)
    public JAXBElement<LIPEventScopeType> createLIPEventTypeScope(LIPEventScopeType value) {
        return new JAXBElement<LIPEventScopeType>(_LIPEventTypeScope_QNAME, LIPEventScopeType.class, LIPEventType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LIPTransferTypeType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LIPTransferTypeType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", name = "TransferType", scope = LIPEventType.class)
    public JAXBElement<LIPTransferTypeType> createLIPEventTypeTransferType(LIPTransferTypeType value) {
        return new JAXBElement<LIPTransferTypeType>(_LIPEventTypeTransferType_QNAME, LIPTransferTypeType.class, LIPEventType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", name = "Periodicity", scope = LIPEventType.class)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    public JAXBElement<String> createLIPEventTypePeriodicity(String value) {
        return new JAXBElement<String>(_LIPEventTypePeriodicity_QNAME, String.class, LIPEventType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NaturalPersonOptType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link NaturalPersonOptType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", name = "NaturalCounterpart", scope = LIPEventType.class)
    public JAXBElement<NaturalPersonOptType> createLIPEventTypeNaturalCounterpart(NaturalPersonOptType value) {
        return new JAXBElement<NaturalPersonOptType>(_LIPEventTypeNaturalCounterpart_QNAME, NaturalPersonOptType.class, LIPEventType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NonNaturalPersonOptType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link NonNaturalPersonOptType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", name = "NonNaturalCounterpart", scope = LIPEventType.class)
    public JAXBElement<NonNaturalPersonOptType> createLIPEventTypeNonNaturalCounterpart(NonNaturalPersonOptType value) {
        return new JAXBElement<NonNaturalPersonOptType>(_LIPEventTypeNonNaturalCounterpart_QNAME, NonNaturalPersonOptType.class, LIPEventType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LIPEventInfoType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LIPEventInfoType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", name = "EventInfo", scope = LIPEventType.class)
    public JAXBElement<LIPEventInfoType> createLIPEventTypeEventInfo(LIPEventInfoType value) {
        return new JAXBElement<LIPEventInfoType>(_LIPEventTypeEventInfo_QNAME, LIPEventInfoType.class, LIPEventType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LIPTaxInfoType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LIPTaxInfoType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", name = "TaxInfo", scope = LIPEventType.class)
    public JAXBElement<LIPTaxInfoType> createLIPEventTypeTaxInfo(LIPTaxInfoType value) {
        return new JAXBElement<LIPTaxInfoType>(_LIPEventTypeTaxInfo_QNAME, LIPTaxInfoType.class, LIPEventType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdjustablePrecisionDateType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AdjustablePrecisionDateType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", name = "StartDate", scope = PENEventType.class)
    public JAXBElement<AdjustablePrecisionDateType> createPENEventTypeStartDate(AdjustablePrecisionDateType value) {
        return new JAXBElement<AdjustablePrecisionDateType>(_PENEventTypeStartDate_QNAME, AdjustablePrecisionDateType.class, PENEventType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdjustablePrecisionDateType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AdjustablePrecisionDateType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", name = "EndDate", scope = PENEventType.class)
    public JAXBElement<AdjustablePrecisionDateType> createPENEventTypeEndDate(AdjustablePrecisionDateType value) {
        return new JAXBElement<AdjustablePrecisionDateType>(_PENEventTypeEndDate_QNAME, AdjustablePrecisionDateType.class, PENEventType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", name = "Periodicity", scope = PENEventType.class)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    public JAXBElement<String> createPENEventTypePeriodicity(String value) {
        return new JAXBElement<String>(_LIPEventTypePeriodicity_QNAME, String.class, PENEventType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReportingTypeType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ReportingTypeType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", name = "ReportingType", scope = PENEventType.class)
    public JAXBElement<ReportingTypeType> createPENEventTypeReportingType(ReportingTypeType value) {
        return new JAXBElement<ReportingTypeType>(_PENEventTypeReportingType_QNAME, ReportingTypeType.class, PENEventType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PENTransferTypeType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PENTransferTypeType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", name = "TransferType", scope = PENEventType.class)
    public JAXBElement<PENTransferTypeType> createPENEventTypeTransferType(PENTransferTypeType value) {
        return new JAXBElement<PENTransferTypeType>(_LIPEventTypeTransferType_QNAME, PENTransferTypeType.class, PENEventType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NaturalPersonOptType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link NaturalPersonOptType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", name = "NaturalCounterpart", scope = PENEventType.class)
    public JAXBElement<NaturalPersonOptType> createPENEventTypeNaturalCounterpart(NaturalPersonOptType value) {
        return new JAXBElement<NaturalPersonOptType>(_LIPEventTypeNaturalCounterpart_QNAME, NaturalPersonOptType.class, PENEventType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NonNaturalPersonOptType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link NonNaturalPersonOptType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", name = "NonNaturalCounterpart", scope = PENEventType.class)
    public JAXBElement<NonNaturalPersonOptType> createPENEventTypeNonNaturalCounterpart(NonNaturalPersonOptType value) {
        return new JAXBElement<NonNaturalPersonOptType>(_LIPEventTypeNonNaturalCounterpart_QNAME, NonNaturalPersonOptType.class, PENEventType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PENEventInfoType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PENEventInfoType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", name = "EventInfo", scope = PENEventType.class)
    public JAXBElement<PENEventInfoType> createPENEventTypeEventInfo(PENEventInfoType value) {
        return new JAXBElement<PENEventInfoType>(_LIPEventTypeEventInfo_QNAME, PENEventInfoType.class, PENEventType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PENTaxInfoType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PENTaxInfoType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:directtax:aeoi:v1", name = "TaxInfo", scope = PENEventType.class)
    public JAXBElement<PENTaxInfoType> createPENEventTypeTaxInfo(PENTaxInfoType value) {
        return new JAXBElement<PENTaxInfoType>(_LIPEventTypeTaxInfo_QNAME, PENTaxInfoType.class, PENEventType.class, value);
    }

}
