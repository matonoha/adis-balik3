//
// This file was generated by the Eclipse Implementation of JAXB, v2.3.3 
// See https://eclipse-ee4j.github.io/jaxb-ri 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.21 at 10:14:16 AM CEST 
//


package eu.europa.ec.taxud.directtax.aeoi.common.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for NameType_Type.
 * 
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="NameType_Type"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&amp;gt;
 *     &amp;lt;enumeration value="indiv"/&amp;gt;
 *     &amp;lt;enumeration value="alias"/&amp;gt;
 *     &amp;lt;enumeration value="nick"/&amp;gt;
 *     &amp;lt;enumeration value="aka"/&amp;gt;
 *     &amp;lt;enumeration value="dba"/&amp;gt;
 *     &amp;lt;enumeration value="legal"/&amp;gt;
 *     &amp;lt;enumeration value="atbirth"/&amp;gt;
 *     &amp;lt;enumeration value="pka"/&amp;gt;
 *     &amp;lt;enumeration value="other"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "NameType_Type")
@XmlEnum
public enum NameTypeType {

    @XmlEnumValue("indiv")
    INDIV("indiv"),
    @XmlEnumValue("alias")
    ALIAS("alias"),
    @XmlEnumValue("nick")
    NICK("nick"),
    @XmlEnumValue("aka")
    AKA("aka"),
    @XmlEnumValue("dba")
    DBA("dba"),
    @XmlEnumValue("legal")
    LEGAL("legal"),
    @XmlEnumValue("atbirth")
    ATBIRTH("atbirth"),
    @XmlEnumValue("pka")
    PKA("pka"),
    @XmlEnumValue("other")
    OTHER("other");
    private final String value;

    NameTypeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static NameTypeType fromValue(String v) {
        for (NameTypeType c: NameTypeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
