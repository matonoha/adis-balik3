//
// This file was generated by the Eclipse Implementation of JAXB, v2.3.3 
// See https://eclipse-ee4j.github.io/jaxb-ri 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.21 at 10:14:16 AM CEST 
//


package eu.europa.ec.taxud.directtax.aeoi.common.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for ContributionTaxTreatment_Type.
 * 
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="ContributionTaxTreatment_Type"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&amp;gt;
 *     &amp;lt;enumeration value="NoRelief"/&amp;gt;
 *     &amp;lt;enumeration value="ReliefDeductible"/&amp;gt;
 *     &amp;lt;enumeration value="ReliefDeducted"/&amp;gt;
 *     &amp;lt;enumeration value="Mixed"/&amp;gt;
 *     &amp;lt;enumeration value="NotApplicable"/&amp;gt;
 *     &amp;lt;enumeration value="Unknown"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "ContributionTaxTreatment_Type")
@XmlEnum
public enum ContributionTaxTreatmentType {

    @XmlEnumValue("NoRelief")
    NO_RELIEF("NoRelief"),
    @XmlEnumValue("ReliefDeductible")
    RELIEF_DEDUCTIBLE("ReliefDeductible"),
    @XmlEnumValue("ReliefDeducted")
    RELIEF_DEDUCTED("ReliefDeducted"),
    @XmlEnumValue("Mixed")
    MIXED("Mixed"),
    @XmlEnumValue("NotApplicable")
    NOT_APPLICABLE("NotApplicable"),
    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown");
    private final String value;

    ContributionTaxTreatmentType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ContributionTaxTreatmentType fromValue(String v) {
        for (ContributionTaxTreatmentType c: ContributionTaxTreatmentType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
