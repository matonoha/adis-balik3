//
// This file was generated by the Eclipse Implementation of JAXB, v2.3.3 
// See https://eclipse-ee4j.github.io/jaxb-ri 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.21 at 10:14:16 AM CEST 
//


package eu.europa.ec.taxud.directtax.aeoi.common.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for MSCurrCode_Type.
 * 
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="MSCurrCode_Type"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="BGN"/&amp;gt;
 *     &amp;lt;enumeration value="CZK"/&amp;gt;
 *     &amp;lt;enumeration value="DKK"/&amp;gt;
 *     &amp;lt;enumeration value="EEK"/&amp;gt;
 *     &amp;lt;enumeration value="EUR"/&amp;gt;
 *     &amp;lt;enumeration value="GBP"/&amp;gt;
 *     &amp;lt;enumeration value="HRK"/&amp;gt;
 *     &amp;lt;enumeration value="HUF"/&amp;gt;
 *     &amp;lt;enumeration value="LTL"/&amp;gt;
 *     &amp;lt;enumeration value="LVL"/&amp;gt;
 *     &amp;lt;enumeration value="PLN"/&amp;gt;
 *     &amp;lt;enumeration value="RON"/&amp;gt;
 *     &amp;lt;enumeration value="SEK"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "MSCurrCode_Type")
@XmlEnum
public enum MSCurrCodeType {

    BGN,
    CZK,
    DKK,
    EEK,
    EUR,
    GBP,
    HRK,
    HUF,
    LTL,
    LVL,
    PLN,
    RON,
    SEK;

    public String value() {
        return name();
    }

    public static MSCurrCodeType fromValue(String v) {
        return valueOf(v);
    }

}
