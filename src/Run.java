import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.ParseException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import _int.eu.cec.taxud.directtax.v3.DirectTaxMessage;
import adis.database.DbConnection;
import adis.database.XmlLIPOdpoved;
import adis.database.XmlOdpoved;
import adis.database.XmlPENOdpoved;
import adis.helpers.Logger;
import adis.helpers.XmlHelper;

public class Run {
	private final static String _url = "http://matonoha.com/xml-corr-pen";
	static Logger console = new Logger();
	
	public static void main(String[] args) throws JAXBException {
		console.log(String.format("Parsing XML from URL %s", _url));
		InputStream file = getInputStreamXML(_url);
		
		DirectTaxMessage message = unmarshall(file);
		DbConnection connection = new DbConnection();
		
		String category = message.getBody().getApplicationId();
		XmlOdpoved response = null;
		
		if(category.equals("AEOI-PEN")) {
			response = new XmlPENOdpoved(message, connection);
		} else if (category.equals("AEOI-LIP")) {
			response = new XmlLIPOdpoved(message, connection);
		}
		
		try {
			response.process();
		} catch (SQLException | ParseException e) {
			e.printStackTrace();
		}
	}
	
	private static DirectTaxMessage unmarshall(InputStream file) throws JAXBException {
		JAXBContext _context = JAXBContext.newInstance("_int.eu.cec.taxud.directtax.eusd.v3:"
				+ "_int.eu.cec.taxud.directtax.v3:"
				+ "eu.europa.ec.taxud.directtax.aeoi.common.v1:"
				+ "eu.europa.ec.taxud.directtax.aeoi.v1"
				+ "oecd.ties.stf.v1");
		Unmarshaller unmarshaller = _context.createUnmarshaller();
		
		return (DirectTaxMessage) unmarshaller.unmarshal(file);
	}
	
	private static InputStream getInputStreamXML(String url) {
		try {
			XmlHelper xmlHelper = new XmlHelper();
			Document xml = xmlHelper.getDocumentFromUrl(url);
			return xmlHelper.getInputStream(xml);
		} catch (SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
		}
		return null;
	}
}
