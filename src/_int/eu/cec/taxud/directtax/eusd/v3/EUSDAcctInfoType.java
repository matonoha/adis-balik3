//
// This file was generated by the Eclipse Implementation of JAXB, v2.3.3 
// See https://eclipse-ee4j.github.io/jaxb-ri 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.21 at 10:14:16 AM CEST 
//


package _int.eu.cec.taxud.directtax.eusd.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import oecd.ties.stf.v1.OBANType;
import oecd.ties.stf.v1.OSINType;


/**
 * &lt;p&gt;Java class for EUSDAcctInfo_Type complex type.
 * 
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="EUSDAcctInfo_Type"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;choice&amp;gt;
 *           &amp;lt;sequence&amp;gt;
 *             &amp;lt;choice&amp;gt;
 *               &amp;lt;element name="IBAN" type="{urn:oecd:ties:stf:v1}IBAN_Type"/&amp;gt;
 *               &amp;lt;element name="OBAN" type="{urn:oecd:ties:stf:v1}OBAN_Type"/&amp;gt;
 *             &amp;lt;/choice&amp;gt;
 *             &amp;lt;element name="SWIFT" type="{urn:oecd:ties:stf:v1}SWIFT_Type" minOccurs="0"/&amp;gt;
 *           &amp;lt;/sequence&amp;gt;
 *           &amp;lt;element name="ISIN" type="{urn:oecd:ties:stf:v1}ISIN_Type"/&amp;gt;
 *           &amp;lt;element name="OSIN" type="{urn:oecd:ties:stf:v1}OSIN_Type"/&amp;gt;
 *           &amp;lt;element name="Other" type="{urn:cec.eu.int:taxud:directtax:eusd:v3}Other_Type"/&amp;gt;
 *         &amp;lt;/choice&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *       &amp;lt;attribute name="shared" type="{urn:cec.eu.int:taxud:directtax:eusd:v3}shared_Type" default="unknown_if_shared" /&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EUSDAcctInfo_Type", propOrder = {
    "iban",
    "oban",
    "swift",
    "isin",
    "osin",
    "other"
})
public class EUSDAcctInfoType {

    @XmlElement(name = "IBAN")
    protected String iban;
    @XmlElement(name = "OBAN")
    protected OBANType oban;
    @XmlElement(name = "SWIFT")
    protected String swift;
    @XmlElement(name = "ISIN")
    protected String isin;
    @XmlElement(name = "OSIN")
    protected OSINType osin;
    @XmlElement(name = "Other")
    protected OtherType other;
    @XmlAttribute(name = "shared")
    protected SharedType shared;

    /**
     * Gets the value of the iban property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIBAN() {
        return iban;
    }

    /**
     * Sets the value of the iban property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIBAN(String value) {
        this.iban = value;
    }

    /**
     * Gets the value of the oban property.
     * 
     * @return
     *     possible object is
     *     {@link OBANType }
     *     
     */
    public OBANType getOBAN() {
        return oban;
    }

    /**
     * Sets the value of the oban property.
     * 
     * @param value
     *     allowed object is
     *     {@link OBANType }
     *     
     */
    public void setOBAN(OBANType value) {
        this.oban = value;
    }

    /**
     * Gets the value of the swift property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSWIFT() {
        return swift;
    }

    /**
     * Sets the value of the swift property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSWIFT(String value) {
        this.swift = value;
    }

    /**
     * Gets the value of the isin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getISIN() {
        return isin;
    }

    /**
     * Sets the value of the isin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setISIN(String value) {
        this.isin = value;
    }

    /**
     * Gets the value of the osin property.
     * 
     * @return
     *     possible object is
     *     {@link OSINType }
     *     
     */
    public OSINType getOSIN() {
        return osin;
    }

    /**
     * Sets the value of the osin property.
     * 
     * @param value
     *     allowed object is
     *     {@link OSINType }
     *     
     */
    public void setOSIN(OSINType value) {
        this.osin = value;
    }

    /**
     * Gets the value of the other property.
     * 
     * @return
     *     possible object is
     *     {@link OtherType }
     *     
     */
    public OtherType getOther() {
        return other;
    }

    /**
     * Sets the value of the other property.
     * 
     * @param value
     *     allowed object is
     *     {@link OtherType }
     *     
     */
    public void setOther(OtherType value) {
        this.other = value;
    }

    /**
     * Gets the value of the shared property.
     * 
     * @return
     *     possible object is
     *     {@link SharedType }
     *     
     */
    public SharedType getShared() {
        if (shared == null) {
            return SharedType.UNKNOWN_IF_SHARED;
        } else {
            return shared;
        }
    }

    /**
     * Sets the value of the shared property.
     * 
     * @param value
     *     allowed object is
     *     {@link SharedType }
     *     
     */
    public void setShared(SharedType value) {
        this.shared = value;
    }

}
