//
// This file was generated by the Eclipse Implementation of JAXB, v2.3.3 
// See https://eclipse-ee4j.github.io/jaxb-ri 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.21 at 10:14:16 AM CEST 
//


package _int.eu.cec.taxud.directtax.eusd.v3;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for partyIdType_Type.
 * 
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="partyIdType_Type"&amp;gt;
 *   &amp;lt;restriction base="{urn:oecd:ties:stf:v1}partyIdType_Type"&amp;gt;
 *     &amp;lt;enumeration value="TIN"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "partyIdType_Type")
@XmlEnum(oecd.ties.stf.v1.PartyIdTypeType.class)
public enum PartyIdTypeType {

    TIN;

    public String value() {
        return name();
    }

    public static PartyIdTypeType fromValue(String v) {
        return valueOf(v);
    }

}
