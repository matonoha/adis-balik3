//
// This file was generated by the Eclipse Implementation of JAXB, v2.3.3 
// See https://eclipse-ee4j.github.io/jaxb-ri 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.07.21 at 10:14:16 AM CEST 
//


package _int.eu.cec.taxud.directtax.eusd.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for CorrectedArt9PaymentData_Type complex type.
 * 
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="CorrectedArt9PaymentData_Type"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;extension base="{urn:cec.eu.int:taxud:directtax:eusd:v3}CorrectableArt9PaymentData_Type"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="BeneficialOwnerDocRefId" type="{urn:cec.eu.int:taxud:directtax:v3}UID_Type" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorrectedArt9PaymentData_Type", propOrder = {
    "beneficialOwnerDocRefId"
})
public class CorrectedArt9PaymentDataType
    extends CorrectableArt9PaymentDataType
{

    @XmlElement(name = "BeneficialOwnerDocRefId")
    protected String beneficialOwnerDocRefId;

    /**
     * Gets the value of the beneficialOwnerDocRefId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeneficialOwnerDocRefId() {
        return beneficialOwnerDocRefId;
    }

    /**
     * Sets the value of the beneficialOwnerDocRefId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeneficialOwnerDocRefId(String value) {
        this.beneficialOwnerDocRefId = value;
    }

}
