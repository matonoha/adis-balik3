package adis.interfaces;

import java.sql.Connection;

public interface IDbConnection {
	Connection getConnection();
	void setConnection(Connection connection);
	void connect();
	void close();
}
