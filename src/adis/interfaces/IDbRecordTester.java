package adis.interfaces;

import java.sql.SQLException;

import adis.database.DbConnection;

public interface IDbRecordTester {
	public boolean recordExists(String table, String whereClause) throws SQLException;
}
