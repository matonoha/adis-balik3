package adis.interfaces;

import java.sql.SQLException;
import java.text.ParseException;

public interface IXmlOdpoved {
	public void process() throws SQLException, ParseException;
}
