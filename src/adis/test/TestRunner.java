package adis.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import adis.test.units.*;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	  //DbTest.class,
	  ConfigParserTest.class,
	  ApiTest.class,
	  XmlParserTest.class
	})
public class TestRunner {
}
