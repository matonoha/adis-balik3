package adis.test.units;

import org.hamcrest.MatcherAssert;
import static org.hamcrest.CoreMatchers.is;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Test;

import adis.database.DbConnection;

public class DbTest {
	private static String command = "_inserts.put(\"%s\", \"INSERT INTO test.%s VALUES (%s)\");";
	
	
	@Test
    public void connectToInformixDatabase() throws SQLException {
		DbConnection connection = new DbConnection();
		connection.connect();
		MatcherAssert.assertThat(connection.isValid(), is(true));
		connection.close();
    }
	
	@Test
	public void getDbTableNames() throws SQLException {
		DbConnection connection = new DbConnection();
		connection.connect();
		DatabaseMetaData metadata = connection.getMetaData();
		
		ResultSet tables = metadata.getTables(null, null, "%", null);
		
		while(tables.next()) {
			String table = tables.getString("TABLE_NAME");
			if(table.contains("t_l_mpd")) {
				ResultSet columns = metadata.getColumns(null, null, table, null);
				int count = 0;
				String columnNames = "";
				
				while(columns.next()) {
					count++;
					columnNames += ":" + columns.getString(4) + ", ";
					//System.out.println(columns.getString(4));
				}
				
				columnNames = columnNames.substring(0, columnNames.length() - 2);
				
				//System.out.println(String.format(command, table, table, columnNames));
				
				if(table.contains("financial_info") || table.contains("dac1_person")) {
					this.print(table, count);
				}
				
				//System.out.println();
				//System.out.println(table);
			}
		}
		
		connection.close();
		MatcherAssert.assertThat(true, is(true));
	}
	
	private void print(String tableName, int numOfColumns) {
		String tmp = "";
		
		for(int i = 0; i < numOfColumns; i++) {
			tmp += "?, ";
		}
		
		tmp = tmp.substring(0, tmp.length() - 2);
		
		System.out.println(String.format(command, tableName, tableName, tmp));
	} 
}
