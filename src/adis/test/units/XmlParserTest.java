package adis.test.units;

import java.io.IOException;
import java.io.InputStream;

import _int.eu.cec.taxud.directtax.v3.DirectTaxMessage;
import adis.factories.PENPayerFactory;
import adis.factories.PENRecipientFactory;
import adis.factories.PENSchemeEventFactory;
import adis.factories.TaxMessageFactory;
import adis.helpers.XmlHelper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;

import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import static org.hamcrest.CoreMatchers.is;

public class XmlParserTest {
	private DirectTaxMessage _message = null;
	private JAXBContext _context = null;

	@Test
	public void parseXmlValues() throws JAXBException {
		InputStream file = getInputStreamXML("http://matonoha.com/xml-corr-pen");
		this._message = this.unmarshall(file);
		
		TaxMessageFactory taxMessage = new TaxMessageFactory(this._message);
		PENRecipientFactory naturalPerson = taxMessage.getRecipientNaturalPerson();
		PENPayerFactory payer = taxMessage.getPayer();
	
		System.out.println("test");
		
		System.out.println(
				naturalPerson.getPersonIdType().getIdType() + "\n" +
				naturalPerson.getPersonIdType().getRef()+ "\n" +
				naturalPerson.getPersonIdType().getCountry() + "\n"
				);
	
		System.out.println(
				naturalPerson.getBirthType().getBirthDate().getDateYMD() + "\n" +
				naturalPerson.getBirthType().getBirthCity() + "\n" +
				naturalPerson.getBirthType().getBirthCountryCode() + "\n"
				);
	
		System.out.println(
				naturalPerson.getAddressType().getAddressTypeType() + "\n" +
				naturalPerson.getAddressType().getCountryCode() + "\n" +
				naturalPerson.getAddressType().getAddressStructType().getStreet() + "\n" +
				naturalPerson.getAddressType().getAddressStructType().getPOB() + "\n" +
				naturalPerson.getAddressType().getAddressStructType().getPostCode() + "\n" +
				naturalPerson.getAddressType().getAddressStructType().getCity() + "\n" +
				naturalPerson.getAddressType().getAddressStructType().getCountrySubentity() + "\n"
				);
	
		System.out.println(
				naturalPerson.getNameType().getNameTypeType() + "\n" +
				naturalPerson.getNameType().getNameStructType().getTitle() + "\n" +
				naturalPerson.getNameType().getNameStructType().getFirstname() + "\n" +
				naturalPerson.getNameType().getNameStructType().getLastName() + "\n"
				);
	
		System.out.println(
				naturalPerson.getActingCapacity() + "\n" +
				naturalPerson.getResidenceCountryCode() + "\n" +
				naturalPerson.getNationalityCountryCode() + "\n" + 
				naturalPerson.getRecipientNumber() + "\n"
				);
	
		System.out.println(
				payer.getPayerTypeType() + "\n" +
				payer.getNonNaturalPerson().getType() + "\n" +
				payer.getNonNaturalPerson().getID().get(0).getIdType() + "\n" +
				payer.getNonNaturalPerson().getID().get(0).getRef() + "\n" +
				payer.getNonNaturalPerson().getID().get(0).getCountry() + "\n"
				);
	
		System.out.println(
				payer.getPersonAddress().getAddressTypeType() + "\n" +
				payer.getPersonAddress().getCountryCode() + "\n" +
				payer.getPersonAddress().getAddressStructType().getStreet() + "\n" +
				payer.getPersonAddress().getAddressStructType().getPOB() + "\n" +
				payer.getPersonAddress().getAddressStructType().getPostCode() + "\n" +
				payer.getPersonAddress().getAddressStructType().getCity() + "\n" +
				payer.getPersonAddress().getAddressStructType().getCountrySubentity() + "\n"
				);

		System.out.println(
				payer.getNonNaturalPerson().getType() + "\n" + 
				payer.getNonNaturalPerson().getPersonName().get(0).getNameType() + "\n" +
				payer.getNonNaturalPerson().getPersonName().get(0).getName()
				);
	
		System.out.println(
				payer.getNonNaturalPerson().getCommencement().getCommencementDate().getDateYMD() + "\n" +
				payer.getNonNaturalPerson().getCommencement().getCommencementCity()  + "\n" +
				payer.getNonNaturalPerson().getCommencement().getCommencementCountrySubentity() + "\n" +
				payer.getNonNaturalPerson().getCommencement().getCommencementCountryCode() + "\n"
				);
	
		System.out.println(
				payer.getNonNaturalPerson().getResidenceCountryCode() + "\n" +
				payer.getNonNaturalPerson().getPermanentEstablishment() + "\n"
				);
	
		System.out.println(
				payer.getNonNaturalRepresentative().getAddress().get(0).getAddressType() + "\n" +
				payer.getNonNaturalRepresentative().getAddress().get(0).getAddressType() + "\n" +
				payer.getNonNaturalRepresentative().getAddress().get(0).getAddressType() + "\n" 
				//payer.getNonNaturalRepresentative().getAddress().get(0).getCountryCode() + "\n"
				);
	
		System.out.println(
				payer.getNonNaturalRepresentative().getPersonName().get(0).getName() + "\n"
				);
	
		System.out.println(
				payer.getNonNaturalRepresentative().getCommencement().getCommencementDate().getDateYMD() + "\n"
				);
	
		System.out.println(
				payer.getScheme().getSchemeType() + "\n" +
				payer.getScheme().getSchemeNature() + "\n" +
				payer.getScheme().getSchemeKind() + "\n" + 
				payer.getScheme().getTaxTreatmentContributions() + "\n"
				);
	
		System.out.println(
				payer.getScheme().getSpecialActivityCategories().getSpecialActivityCategory().get(0)
				);
	
		System.out.println(
				payer.getScheme().getReferenceInfos().getReferenceInfo().get(0).getSchemeReferenceType() + "\n" +
				payer.getScheme().getReferenceInfos().getReferenceInfo().get(0).getSchemeReference() + "\n"
				);
	
		System.out.println(
				payer.getScheme().getCapitalValues().getCapitalValue().get(0).getSchemeCapitalValueType() + "\n" + 
				payer.getScheme().getCapitalValues().getCapitalValue().get(0).getAmount().getCurrency() + "\n" +
				payer.getScheme().getCapitalValues().getCapitalValue().get(0).getDate().getDateYMD() + "\n" +
				payer.getScheme().getTaxationRightSource() + "\n" +
				payer.getScheme().isForeignApprovedScheme() + "\n"
				);
	
		PENSchemeEventFactory schemeEvent = new PENSchemeEventFactory(payer.getScheme().getEvents().getEvent().get(0));
		System.out.println(
				schemeEvent.getEventType() + "\n" +
				schemeEvent.getEventInfoType().getTaxationTypeEvent().value() + "\n" + 
				schemeEvent.getEventInfoType().isExistence() + "\n" 
				);
	
		System.out.println(
				taxMessage.getDocSpec().getDocTypeIndic() + "\n" +
				taxMessage.getDocSpec().getDocRefId() + "\n" +
				taxMessage.getDocSpec().getCorrDocRefId() + "\n" + 
				schemeEvent.getEventType()
				);
		
		System.out.println(
				schemeEvent.isEventExistence() + "\n" +
						schemeEvent.getTaxationTypeTax() + "\n" +
						schemeEvent.getTaxableBasisType()
				);
		
		MatcherAssert.assertThat(naturalPerson.getRecipientNumber(), is(String.class));
	}
	
	private DirectTaxMessage unmarshall(InputStream file) throws JAXBException {
		_context = JAXBContext.newInstance("_int.eu.cec.taxud.directtax.eusd.v3:"
				+ "_int.eu.cec.taxud.directtax.v3:"
				+ "eu.europa.ec.taxud.directtax.aeoi.common.v1:"
				+ "eu.europa.ec.taxud.directtax.aeoi.v1"
				+ "oecd.ties.stf.v1");
		Unmarshaller unmarshaller = _context.createUnmarshaller();
		
		return (DirectTaxMessage) unmarshaller.unmarshal(file);
	}
	
	private InputStream getInputStreamXML(String url) {
		try {
			XmlHelper xmlHelper = new XmlHelper();
			Document xml = xmlHelper.getDocumentFromUrl(url);
			return xmlHelper.getInputStream(xml);
		} catch (SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}

