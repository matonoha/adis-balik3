package adis.test.units;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Test;

public class ApiTest {
	@Test
	public void connectToXmlGetApi() throws ConnectException, IOException {
		URL url = null;
		HttpURLConnection connection = null;
		
		url = new URL("http://matonoha.com/xml-corr");
		connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Accept", "application/xml");
		connection.connect();
			
		assertEquals(HttpURLConnection.HTTP_OK, connection.getResponseCode());
	}
}
