package adis.test.units;

import static org.hamcrest.CoreMatchers.is;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsMapContaining;
import org.junit.Test;

import adis.helpers.DbConfigParser;

public class ConfigParserTest {

	@Test
	public void parseConfigWithoutParam() throws FileNotFoundException{
		DbConfigParser parser = new DbConfigParser();
		HashMap<String, String> expectedResult = getExpectedMap();
		HashMap<String, String> result = parser.getConfigMap();
			
		Set<String> keys = expectedResult.keySet();
		Iterator<String> i = keys.iterator();
			
		MatcherAssert.assertThat(result.size(), is(expectedResult.size()));
			
		while(i.hasNext()) {
			MatcherAssert.assertThat(result, IsMapContaining.hasKey(i.next()));
		}
		
	}
	
	@Test
	public void parseConfigWithBufferedReaderParam() throws FileNotFoundException {
		String basePath = new File("").getAbsolutePath();
		basePath += "/files/db_config.json";
		BufferedReader reader = new BufferedReader(new FileReader(basePath));
		DbConfigParser parser = new DbConfigParser(reader);
		HashMap<String, String> expectedResult = getExpectedMap();
		HashMap<String, String> result = parser.getConfigMap();
			
		Set<String> keys = expectedResult.keySet();
		Iterator<String> i = keys.iterator();
			
		MatcherAssert.assertThat(result.size(), is(expectedResult.size()));
			
		while(i.hasNext()) {
			MatcherAssert.assertThat(result, IsMapContaining.hasKey(i.next()));
		}
	}
	
	@Test
	public void parseConfigWithFilePathStringParam() throws FileNotFoundException {
		String basePath = new File("").getAbsolutePath();
		basePath += "/files/db_config.json";
		DbConfigParser parser = new DbConfigParser(basePath);
		HashMap<String, String> expectedResult = getExpectedMap();
		HashMap<String, String> result = parser.getConfigMap();
			
		Set<String> keys = expectedResult.keySet();
		Iterator<String> i = keys.iterator();
			
		MatcherAssert.assertThat(result.size(), is(expectedResult.size()));
			
		while(i.hasNext()) {
			MatcherAssert.assertThat(result, IsMapContaining.hasKey(i.next()));
		}
	}
	
	private HashMap<String, String> getExpectedMap() {
		HashMap<String, String> tmp = new HashMap<String, String>();
		tmp.put("db_host", "");
		tmp.put("db_server", "");
		tmp.put("db_name", "");
		tmp.put("db_username", "");
		tmp.put("db_password", "");
		tmp.put("db_port", "");
		
		return tmp;
	}
}
