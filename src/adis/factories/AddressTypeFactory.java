package adis.factories;

import java.util.List;

import javax.xml.bind.JAXBElement;

import eu.europa.ec.taxud.directtax.aeoi.common.v1.AddressStructType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.AddressType;

public class AddressTypeFactory {
	private AddressType _addressType;
	private String _countryCode;
	private String _addressFree;
	private AddressStructType _addressStructType;
	
	public AddressTypeFactory(AddressType addressType) {
		this._addressType = addressType;
		List<JAXBElement<?>> elements = addressType.getContent();
		Object obj = null;
		String className = "";
		
		for (JAXBElement<?> item : elements) {
			obj = item.getValue();
			className = obj.getClass().getSimpleName();
			
			switch(className) {
			case "AddressStructType":
				this._addressStructType = (AddressStructType) obj;
				break;
			case "String":
				if(item.getName().getLocalPart().contains("Country")) {
					this._countryCode = (String) obj;
				} else {
					this._addressFree = (String) obj;
				}
				break;
			}
		}
	}
	
	public AddressStructType getAddressStructType() {
		return this._addressStructType;
	}
	
	public String getAddressTypeType() {
		return this._addressType.getAddressType().name();
	}
	
	public String getCountryCode() {
		return this._countryCode;
	}
	
	public String getAddressFree() {
		return this._addressFree;
	}
}
