package adis.factories;

import java.util.List;

import javax.xml.bind.JAXBElement;

import eu.europa.ec.taxud.directtax.aeoi.common.v1.NameStructType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.NameType;

public class NameTypeFactory {
	private NameType _nameType;
	private NameStructType _nameStructType;
	private String _nameFree;
	
	public NameTypeFactory(NameType nameType) {
		Object obj = null;
		String className = "";
		this._nameType = nameType;
		List<JAXBElement<?>> elements = nameType.getContent();
		
		for(JAXBElement<?> item : elements) {
			obj = item.getValue();
			className = obj.getClass().getSimpleName();
			
			switch(className) {
			case "NameStructType":
				this._nameStructType = (NameStructType) obj;
				break;
			case "String":
				System.out.println("aaa");
				this._nameFree = (String) obj;
				break;
			}
		}
	}
	
	public NameStructType getNameStructType() {
		return this._nameStructType;
	}
	
	public String getNameTypeType() {
		return this._nameType.getNameType().name();
	}
	
	public String getNameFree() {
		return this._nameFree;
	}
}
