package adis.factories;

import java.util.List;

import javax.xml.bind.JAXBElement;

import eu.europa.ec.taxud.directtax.aeoi.common.v1.AdjustablePrecisionDateType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.NaturalPersonOptType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.NonNaturalPersonOptType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.ReportingTypeType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.TaxableBasisTypeType;
import eu.europa.ec.taxud.directtax.aeoi.v1.PENEventInfoType;
import eu.europa.ec.taxud.directtax.aeoi.v1.PENEventType;
import eu.europa.ec.taxud.directtax.aeoi.v1.PENEventTypeType;
import eu.europa.ec.taxud.directtax.aeoi.v1.PENTaxInfoType;
import eu.europa.ec.taxud.directtax.aeoi.v1.PENTaxType;
import eu.europa.ec.taxud.directtax.aeoi.v1.PENTaxationTypeTaxType;
import eu.europa.ec.taxud.directtax.aeoi.v1.PENTransferTypeType;

public class PENSchemeEventFactory {
	private PENEventType _penEventType;
	private AdjustablePrecisionDateType _startDate;
	private AdjustablePrecisionDateType _endDate;
	private ReportingTypeType _reportingTypeType;
	private PENTransferTypeType _transferTypeType;
	private NaturalPersonOptType _naturalPersonOptType;
	private NonNaturalPersonOptType _nonNaturalPersonOptType;
	private PENEventInfoType _eventInfoType;
	private PENTaxInfoType _taxInfoType;
	private String periodicity;
	
	public PENSchemeEventFactory(PENEventType eventType) {
		this._penEventType = eventType;
		
		this.parseContent(this._penEventType.getContent());
	}
	
	private void parseContent(List<JAXBElement<?>> elements) {
		Object obj = null;
		String className = "";
		
		for(JAXBElement<?> item : elements) {
			obj = item.getValue();
			className = obj.getClass().getSimpleName();
			
			switch(className) {
			case "AdjustablePrecisionDateType": // StartDate and EndDate
				if(item.getName().getLocalPart().contains("Start")) {
					this._startDate = (AdjustablePrecisionDateType) obj;
				} else {
					this._endDate = (AdjustablePrecisionDateType) obj;
				}
				break;
			case "ReportingTypeType":
				this._reportingTypeType = (ReportingTypeType) obj;
				break;
			case "PENTransferTypeType":
				this._transferTypeType = (PENTransferTypeType) obj;
				break;
			case "NaturalPersonOptType":
				this._naturalPersonOptType = (NaturalPersonOptType) obj;
				break;
			case "NonNaturalPersonOptType":
				this._nonNaturalPersonOptType = (NonNaturalPersonOptType) obj;
				break;
			case "PENEventInfoType":
				this._eventInfoType = (PENEventInfoType) obj;
				break;
			case "PENTaxInfoType":
				this._taxInfoType = (PENTaxInfoType) obj;
				break;
			case "String":
				if(item.getName().getLocalPart().contains("Periodicity")) {
					this.periodicity = (String) obj;
				}
				break;
			}
		}
	}
	
	public String getEventType() {
		if(_penEventType == null) return null;
		return this._penEventType.getEventType().value();
	}

	public PENEventType getPenEventType() {
		return _penEventType;
	}

	public String getStartDate() {
		if(_startDate != null) {
			return _startDate.getDateYMD().toString();
		}
		return null;
	}

	public String getEndDate() {
		if(_endDate != null) {
			return _endDate.getDateYMD().toString();
		}
		return null;
	}

	public String getReportingType() {
		if(_reportingTypeType == null) return null;
		return _reportingTypeType.value();
	}

	public String getTransferType() {
		if(_transferTypeType == null) return null;
		return _transferTypeType.value();
	}

	public NaturalPersonOptType getNaturalPersonOptType() {
		return _naturalPersonOptType;
	}

	public NonNaturalPersonOptType getNonNaturalPersonOptType() {
		return _nonNaturalPersonOptType;
	}

	public PENEventInfoType getEventInfoType() {
		return _eventInfoType;
	}
	
	public String getEventBasisType() {
		if(_eventInfoType.getEventBasisType() == null) return null;
		return this._eventInfoType.getEventBasisType().value();
	}
	
	public String getTaxableBasisType() {
		if(_taxInfoType == null) return null;
		return this._taxInfoType.getTaxableBasisType().value();
	}

	public PENTaxInfoType getTaxInfoType() {
		return _taxInfoType;
	}

	public String getPeriodicity() {
		return periodicity;
	}
	
	public String getTaxationTypeTax() {
		if(_taxInfoType == null) return null;
		return this._taxInfoType.getTaxationTypeTax().value();
	}
	
	public boolean isEventExistence() {
		return _eventInfoType.isExistence();
	}
	
	public String getTax() {
		if(_taxInfoType == null) return null;
		return this._taxInfoType.getTax().value();
	}
}
