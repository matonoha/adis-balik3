package adis.factories;

import java.util.List;

import eu.europa.ec.taxud.directtax.aeoi.v1.LIPEventType;
import eu.europa.ec.taxud.directtax.aeoi.v1.LIPPolicyType;
import eu.europa.ec.taxud.directtax.aeoi.v1.LIPPolicyType.Beneficiaries;

public class LIPPolicyFactory {
	private LIPPolicyType _policyType;
	
	public LIPPolicyFactory(List<LIPPolicyType> policies) {
		setPolicyType(policies.get(0));	
	}
	
	public LIPPolicyType getPolicyType() {
		return this._policyType;
	}
	
	private void setPolicyType(LIPPolicyType value) {
		this._policyType = value;
	}
	
	public Beneficiaries getBeneficiaries() {
		return this.getPolicyType().getBeneficiaries();
	}
	
	public LIPEventType getEvents() {
		return getPolicyType().getEvents().getEvent().get(0);
	}
	
	public String getEndDate() {
		if(getPolicyType().getStartDate() == null) return null;
		return getPolicyType().getStartDate().getDateYMD().toString();
	}
	
	public String getStartDate() {
		if(getPolicyType().getEndDate() == null) return null;
		return getPolicyType().getEndDate().getDateYMD().toString();
	}
	
	public String getOtherInfo() {
		if(getPolicyType().getOtherInfo().size() == 0) return null;
		return getPolicyType().getOtherInfo().get(0).getValue();
	}
}