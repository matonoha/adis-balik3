package adis.factories;

import eu.europa.ec.taxud.directtax.aeoi.v1.LIPClassBeneficiaryType;
import eu.europa.ec.taxud.directtax.aeoi.v1.LIPNamedBeneficiaryType;
import eu.europa.ec.taxud.directtax.aeoi.v1.LIPPolicyType.Beneficiaries;

public class LIPBeneficiaryFactory {
	private LIPClassBeneficiaryType classBeneficiary = null;
	private LIPNamedBeneficiaryType namedBeneficiary = null;

	public LIPBeneficiaryFactory(Beneficiaries beneficiaries) {
		if (beneficiaries.getClassBeneficiary() != null) this.setClassBeneficiary(beneficiaries.getClassBeneficiary());
		if (beneficiaries.getNamedBeneficiary() != null) this.setNamedBeneficiary(beneficiaries.getNamedBeneficiary());
	}
	
	public LIPClassBeneficiaryType getClassBeneficiary() {
		return classBeneficiary;
	}

	public LIPNamedBeneficiaryType getNamedBeneficiary() {
		return namedBeneficiary;
	}

	public void setClassBeneficiary(LIPClassBeneficiaryType classBeneficiary) {
		this.classBeneficiary = classBeneficiary;
	}

	public void setNamedBeneficiary(LIPNamedBeneficiaryType namedBeneficiary) {
		this.namedBeneficiary = namedBeneficiary;
	}
}
