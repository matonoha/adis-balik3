package adis.factories;

import java.util.List;

import javax.xml.bind.JAXBElement;

import eu.europa.ec.taxud.directtax.aeoi.common.v1.AddressType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.BirthType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.CapacityType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.I18NStringType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.NameType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.PersonIdType;
import eu.europa.ec.taxud.directtax.aeoi.v1.PENRecipientType;
import oecd.ties.stf.v1.GenderType;

public class PENRecipientFactory {
	private BirthType _birthType;
	private PersonIdType _personIdType;
	private AddressType _addressType;
	private NameType _nameType;
	private CapacityType _capacityType;
	private GenderType _genderType;
	private String _residenceCountryCode;
	private String _nationalityCountryCode;
	private I18NStringType _i18NStringType;
	private PENRecipientType _recipient;
	
	public PENRecipientFactory(List<PENRecipientType> recipients) {
		_recipient = recipients.get(0);
		List<JAXBElement<?>> elements = _recipient.getNaturalPerson().getContent();
		Object obj = null;
		String className = "";
		
		for (JAXBElement<?> item : elements) {
			obj = item.getValue();
			className = obj.getClass().getSimpleName();
			
			switch(className) {
			case "BirthType":
				this.setBirthType((BirthType) obj);
				break;
			case "PersonIdType":
				this.setPersonIdType((PersonIdType) obj);
				break;
			case "AddressType":
				this.setAddressType((AddressType) obj);
				break;
			case "NameType":
				this.setNameType((NameType) obj);
				break;
			case "CapacityType":
				this.setActingCapacity((CapacityType) obj);
				break;
			case "GenderType":
				this.setGenderType((GenderType) obj);
				break;
			case "I18NStringType":
				this.setI18NStringType((I18NStringType) obj);
				break;
			case "String":
				String elementName = item.getName().getLocalPart();
				if(elementName.contains("Residence")) {
					this.setResidenceCountryCode((String) obj);
				} else if(elementName.contains("Nationality")) {
					this.setNationalityCountryCode((String) obj);
				}
				break;
			}
		}
	}
	
	public String getRecipientNumber() {
		return this._recipient.getRecipientNumber();
	}
	
	public BirthType getBirthType() {
		return _birthType;
	}

	public void setBirthType(BirthType birthType) {
		this._birthType = birthType;
	}

	public PersonIdType getPersonIdType() {
		return _personIdType;
	}

	public void setPersonIdType(PersonIdType personIdType) {
		this._personIdType = personIdType;
	}

	public AddressTypeFactory getAddressType() {
		return new AddressTypeFactory(this._addressType);
	}

	public void setAddressType(AddressType addressType) {
		this._addressType = addressType;
	}

	public NameTypeFactory getNameType() {
		return new NameTypeFactory(this._nameType);
	}

	public void setNameType(NameType nameType) {
		this._nameType = nameType;
	}

	public CapacityType getActingCapacity() {
		return _capacityType;
	}

	public void setActingCapacity(CapacityType capacityType) {
		this._capacityType = capacityType;
	}

	public GenderType getGenderType() {
		return _genderType;
	}

	public void setGenderType(GenderType genderType) {
		this._genderType = genderType;
	}

	public String getResidenceCountryCode() {
		return _residenceCountryCode;
	}

	public void setResidenceCountryCode(String residenceCountryCode) {
		this._residenceCountryCode = residenceCountryCode;
	}
	
	public String getNationalityCountryCode() {
		return this._nationalityCountryCode;
	}
	
	public void setNationalityCountryCode(String nationalityCountryCode) {
		this._nationalityCountryCode = nationalityCountryCode;
	}

	public I18NStringType getI18NStringType() {
		return _i18NStringType;
	}

	public void setI18NStringType(I18NStringType i18NStringType) {
		this._i18NStringType = i18NStringType;
	}
}
