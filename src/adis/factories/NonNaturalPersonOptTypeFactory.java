package adis.factories;

import java.util.List;

import eu.europa.ec.taxud.directtax.aeoi.common.v1.AddressOptType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.CommencementType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.NonNaturalPersonOptType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.PersonIdOptType;

public class NonNaturalPersonOptTypeFactory {
	private NonNaturalPersonOptType _nonNaturalPersonOptType = null;
	
	public NonNaturalPersonOptTypeFactory(NonNaturalPersonOptType nonNaturalPersonOptType) {
		this._nonNaturalPersonOptType = nonNaturalPersonOptType;
		
		nonNaturalPersonOptType.getID();
		nonNaturalPersonOptType.getAddress();
		nonNaturalPersonOptType.getCommencement();
		nonNaturalPersonOptType.getForm();
		nonNaturalPersonOptType.getOtherPartyInfo();
		nonNaturalPersonOptType.getPermanentEstablishment();
		nonNaturalPersonOptType.getPersonName();
		nonNaturalPersonOptType.getResidenceCountryCode();
		nonNaturalPersonOptType.getType();
	}
	
	public List<PersonIdOptType> getID() {
		return this._nonNaturalPersonOptType.getID();
	}
	
	public AddressOptType getAddress() {
		return this._nonNaturalPersonOptType.getAddress().get(0);
	}
	
	public CommencementType getCommencement() {
		return this._nonNaturalPersonOptType.getCommencement();
	}
}
