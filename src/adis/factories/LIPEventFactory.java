package adis.factories;

import java.util.List;

import javax.xml.bind.JAXBElement;

import eu.europa.ec.taxud.directtax.aeoi.common.v1.AdjustablePrecisionDateType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.NaturalPersonOptType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.NonNaturalPersonOptType;
import eu.europa.ec.taxud.directtax.aeoi.v1.LIPEventInfoType;
import eu.europa.ec.taxud.directtax.aeoi.v1.LIPEventScopeType;
import eu.europa.ec.taxud.directtax.aeoi.v1.LIPEventType;
import eu.europa.ec.taxud.directtax.aeoi.v1.LIPTaxInfoType;
import eu.europa.ec.taxud.directtax.aeoi.v1.LIPTaxType;
import eu.europa.ec.taxud.directtax.aeoi.v1.LIPTransferTypeType;

public class LIPEventFactory {
	private AdjustablePrecisionDateType _actualDate = null;
	private AdjustablePrecisionDateType _deemedDate = null;
	private LIPEventScopeType _scope;
	private LIPTransferTypeType _transfer;
	private String _periodicity;
	private NaturalPersonOptType _naturalCounterpart;
	private NonNaturalPersonOptType _nonNaturalCounterpart;
	private LIPEventInfoType _eventInfo;
	private LIPTaxInfoType _taxInfo;
	private LIPEventType _eventType;
	
	public LIPEventFactory(LIPEventType eventType) {
		List<JAXBElement<?>> elements = eventType.getContent();
		
		Object obj = null;
		String className = "";
		
		for(JAXBElement<?> item : elements) {
			obj = item.getValue();
			className = obj.getClass().getSimpleName();
			System.out.println(className);
			
			switch(className) {
			case "AdjustablePrecisionDateType":
				if(item.getName().getLocalPart().contains("Deemed")) {
					this.setDeemedDate((AdjustablePrecisionDateType) obj);
				} else {
					this.setActualDate((AdjustablePrecisionDateType) obj);
				}
				break;
				
			case "LIPEventScopeType":
				this.setScope((LIPEventScopeType) obj);
				break;
				
			case "LIPTransferTypeType":
				this.setTransfer((LIPTransferTypeType) obj);
				break;
				
			case "String":
				this.setPeriodicity((String) obj);
				break;
				
			case "NaturalPersonOptType":
				this.setNaturalCounterpart((NaturalPersonOptType) obj);
				break;
				
			case "NonNaturalPersonOptType":
				this.setNonNaturalCounterpart((NonNaturalPersonOptType) obj);
				break;
				
			case "LIPEventInfoType":
				this.setEventInfo((LIPEventInfoType) obj);
				break;
				
			case "LIPTaxInfoType":
				this.setTaxInfo((LIPTaxInfoType) obj);
				break;
			}
		}
	}

	public AdjustablePrecisionDateType getActualDate() {
		return _actualDate;
	}

	public AdjustablePrecisionDateType getDeemedDate() {
		return _deemedDate;
	}

	public LIPEventScopeType getScope() {
		return _scope;
	}

	public LIPTransferTypeType getTransfer() {
		return _transfer;
	}

	public String getPeriodicity() {
		return _periodicity;
	}

	public NaturalPersonOptType getNaturalCounterpart() {
		return _naturalCounterpart;
	}

	public NonNaturalPersonOptType getNonNaturalCounterpart() {
		return _nonNaturalCounterpart;
	}

	public LIPEventInfoType getEventInfo() {
		return _eventInfo;
	}

	public LIPTaxInfoType getTaxInfo() {
		return _taxInfo;
	}

	public LIPEventType getEventType() {
		return _eventType;
	}

	public void setActualDate(AdjustablePrecisionDateType actualDate) {
		this._actualDate = actualDate;
	}

	public void setDeemedDate(AdjustablePrecisionDateType deemedDate) {
		this._deemedDate = deemedDate;
	}

	public void setScope(LIPEventScopeType scope) {
		this._scope = scope;
	}

	public void setTransfer(LIPTransferTypeType transfer) {
		this._transfer = transfer;
	}

	public void setPeriodicity(String periodicity) {
		this._periodicity = periodicity;
	}

	public void setNaturalCounterpart(NaturalPersonOptType naturalCounterpart) {
		this._naturalCounterpart = naturalCounterpart;
	}

	public void setNonNaturalCounterpart(NonNaturalPersonOptType nonNaturalCounterpart) {
		this._nonNaturalCounterpart = nonNaturalCounterpart;
	}

	public void setEventInfo(LIPEventInfoType eventInfo) {
		this._eventInfo = eventInfo;
	}

	public void setTaxInfo(LIPTaxInfoType taxInfo) {
		this._taxInfo = taxInfo;
	}

	public void setEventType(LIPEventType eventType) {
		this._eventType = eventType;
	}
	
	public LIPTaxType getTaxType() {
		if(getTaxInfo() == null) return null;
		return getTaxInfo().getTax();
	}
}
