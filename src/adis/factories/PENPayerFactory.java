package adis.factories;

import java.util.List;

import eu.europa.ec.taxud.directtax.aeoi.common.v1.AddressType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.NonNaturalPersonOptType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.NonNaturalPersonType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.PayerTypeType;
import eu.europa.ec.taxud.directtax.aeoi.v1.PENPayerType;
import eu.europa.ec.taxud.directtax.aeoi.v1.PENSchemeType;

public class PENPayerFactory {
	private NonNaturalPersonType _nonNaturalPersonType;
	private AddressType _personAddressType;
	private PayerTypeType _payerTypeType;
	private NonNaturalPersonOptType _nonNaturalRepresentative;
	private PENSchemeType _schemeType;
	private boolean _formerEmployer;
	private boolean _insuranceCompany;
	private List<PENPayerType> _payers;
	
	public PENPayerFactory(List<PENPayerType> payers) {
		setPayers(payers);
		this._personAddressType = payers.get(0).getNonNaturalPerson().getAddress().get(0);
		this._nonNaturalPersonType = payers.get(0).getNonNaturalPerson();
		this._payerTypeType = payers.get(0).getPayerType();
		this._nonNaturalRepresentative = payers.get(0).getNonNaturalRepresentative();
		this._schemeType = payers.get(0).getSchemes().getScheme().get(0);
		//this._formerEmployer = payers.get(0).isFormerEmployer();
		//this._insuranceCompany = payers.get(0).isInsuranceCompany();
		this._formerEmployer = false;
		this._insuranceCompany = false;
	}
	
	public NonNaturalPersonType getNonNaturalPerson() {
		return this._nonNaturalPersonType;
	}
	
	public List<PENPayerType> getPayers() {
		return this._payers;
	}
	
	private void setPayers(List<PENPayerType> payers) {
		this._payers = payers;
	}
	
	public AddressTypeFactory getPersonAddress() {
		return new AddressTypeFactory(this._personAddressType);
	}
	
	public PayerTypeType getPayerTypeType() {
		return this._payerTypeType;
	}
	
	public NonNaturalPersonOptType getNonNaturalRepresentative() {
		return this._nonNaturalRepresentative;
	}
	
	public PENSchemeType getScheme() {
		return this._schemeType;
	}
	
	public boolean isFormerEmployer() {
		return this._formerEmployer;
	}
	
	public boolean isInsuranceCompany() {
		return this._insuranceCompany;
	}
}