package adis.factories;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import _int.eu.cec.taxud.directtax.eusd.v3.DocSpecType;
import _int.eu.cec.taxud.directtax.v3.DirectTaxMessage;
import _int.eu.cec.taxud.directtax.v3.HeaderType;
import eu.europa.ec.taxud.directtax.aeoi.common.v1.NaturalPersonType;
import eu.europa.ec.taxud.directtax.aeoi.v1.AEOILIPBodyType;
import eu.europa.ec.taxud.directtax.aeoi.v1.AEOIPENBodyType;

public class TaxMessageFactory {
	private DirectTaxMessage _directTaxMessage;
	private NaturalPersonType _naturalPersonType;
	private PENRecipientFactory _recipientFactory;
	private PENPayerFactory _payerFactory;
	private DocSpecType _docSpec;
	
	private AEOILIPBodyType _bodyLIP;
	private AEOIPENBodyType _bodyPEN;
	
	private LIPBeneficiaryFactory _beneficiaryFactory;
	private LIPPolicyFactory _policyFactory;
	
	public TaxMessageFactory(DirectTaxMessage directTaxMessage) {
		this._directTaxMessage = directTaxMessage;
		String category = directTaxMessage.getBody().getApplicationId().split("-")[1];
		
		if(category.equals("PEN")) {
			_bodyPEN = (AEOIPENBodyType) directTaxMessage.getBody();

			setRecipientFactory(new PENRecipientFactory(_bodyPEN.getRecipients()));
			setNaturalPersonType(_bodyPEN.getRecipients().get(0).getNaturalPerson());
			setPayerFactory(new PENPayerFactory(_bodyPEN.getRecipients().get(0).getPayers().getPayer()));
			setDocSpec(_bodyPEN.getRecipients().get(0).getDocSpec());
			
		} else if (category.equals("LIP")) {
			_bodyLIP = (AEOILIPBodyType) directTaxMessage.getBody();

			setPolicyFactory(new LIPPolicyFactory(_bodyLIP.getPolicies()));
			setBeneficiaryFactory(new LIPBeneficiaryFactory(getPolicyFactory().getBeneficiaries()));
		}
	}
	
	private void setRecipientFactory(PENRecipientFactory value) {
		_recipientFactory = value;
	}
	
	private void setPayerFactory(PENPayerFactory value) {
		_payerFactory = value;
	}
	
	public PENPayerFactory getPayerFactory() {
		return _payerFactory;
	}
	
	private void setNaturalPersonType(NaturalPersonType value) {
		_naturalPersonType = value;
	}
	
	public LIPBeneficiaryFactory getBeneficiaryFactory() {
		return _beneficiaryFactory;
	}

	public LIPPolicyFactory getPolicyFactory() {
		return _policyFactory;
	}

	public void setBeneficiaryFactory(LIPBeneficiaryFactory _beneficiaryFactory) {
		this._beneficiaryFactory = _beneficiaryFactory;
	}

	public void setPolicyFactory(LIPPolicyFactory _policyFactory) {
		this._policyFactory = _policyFactory;
	}

	public PENRecipientFactory getRecipientNaturalPerson() {
		return this._recipientFactory;
	}
	
	public HeaderType getHeader() {
		return this._directTaxMessage.getHeader();
	}
	
	public String getBodyApplicationId() {
		return this._directTaxMessage.getBody().getApplicationId();
	}
	
	public XMLGregorianCalendar getBodyTaxYear() {
		return ((AEOIPENBodyType) _directTaxMessage.getBody()).getTaxYear();
	}
	
	public PENPayerFactory getPayer() {
		return this._payerFactory;
	}
	
	public DocSpecType getDocSpec() {
		return this._docSpec;
	}
	
	private void setDocSpec(DocSpecType value) {
		_docSpec = value;
	}
	
	public PENSchemeEventFactory getSchemeEvent() {
		return new PENSchemeEventFactory(getPayer().getScheme().getEvents().getEvent().get(0));
	}
}
