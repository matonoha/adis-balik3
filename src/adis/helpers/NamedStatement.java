package adis.helpers;

import java.math.BigDecimal;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class NamedStatement {
	private String _sql;
	
	public NamedStatement(String sql) {
		setSQL(sql);
	}
	
	public void setValue(String columnName, Object value) {
		String[] values = getSQL().split(",");
		String column = ":" + columnName;
		
		for(int i = 0; i < values.length; i++) {
			if(values[i].contains(column)) {
                values[i] = values[i].replace(column, format(value));
                break;
            }
		}
		setSQL(String.join(",", values));
	}
	
	private String format(Object obj) {
		if(obj == null) return "NULL";
		
		String tmp = obj.getClass().getSimpleName();
		String result = "";
		
		switch(tmp) {
		case "String":
			result = ((obj != null) ? "'" + (String) obj.toString() + "'" : "NULL");
			break;
		case "Integer":
			result = ((obj != null) ? ((Integer) obj).toString() : "NULL");
			break;
		case "Long":
			result = ((obj != null) ? ((Long) obj).toString() : "NULL");
			break;
		case "Double":
			result = ((obj != null) ? ((Double) obj).toString() : "NULL");
			break;
		case "Float":
			result = ((obj != null) ? ((Float) obj).toString(): "NULL");
			break;
		case "Timestamp":
			result = ((obj != null) ? "'" + ((Timestamp) obj).toString().replaceAll("/\\.(.*)", "") + "'" : "NULL");
			break;
		case "LocalDateTime":
			result = ((obj != null) ? "'" + ((LocalDateTime) obj).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + "'" : "NULL");
			break;
		case "Date":
			result = ((obj != null) ? "'" + ((Date) obj).toString() + "'" : "NULL");
			break;
		case "Byte":
			result = ((obj != null) ? ((Byte) obj).toString() : "NULL"); 
			break;
		case "Boolean":
			result = ((obj != null) ? (((Boolean) obj) ? "1" : "0") : "NULL");
			break;
		case "BigDecimal":
			result = ((obj != null) ? ((BigDecimal) obj).toString() : "NULL");
		default:
			result = "'" + obj.toString() + "'";
			break;
		}
		
		return result;
	}
	
	
	public String getSQL() {
		return this._sql;
	}
	
	public void setSQL(String value) {
		this._sql = value;
	}
}