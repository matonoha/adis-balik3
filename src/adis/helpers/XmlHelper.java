package adis.helpers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class XmlHelper {
	private TransformerFactory _transFactory = null;
	
	public XmlHelper() {
		_transFactory = TransformerFactory.newInstance();
	}
	
	/**
	 * Returns a String object from a Document.
	 * @param document w3c Document to be parsed into a String
	 * */
	public String getString(Document document) {
		try {
			StringWriter stringWriter = new StringWriter();
			Transformer transformer = _transFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			
			transformer.transform(new DOMSource(document), new StreamResult(stringWriter));
			return stringWriter.toString();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Returns a w3c Document from a specified URL
	 * @param url URL to get the XML from
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * */
	public Document getDocumentFromUrl(String url) throws IOException, ParserConfigurationException, SAXException {
			URL link = new URL(url);
			HttpURLConnection connection = (HttpURLConnection) link.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/xml");

			InputStream xml = connection.getInputStream();

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(xml);
			connection.disconnect();
			
			return doc;
	}
	
	/**
	 * Returns an InputStream converted from a w3c Document.
	 * @param document w3c Document object
	 * */
	public InputStream getInputStream(Document document) {
		ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
		Source xmlSource = new DOMSource(document);
		Result result = new StreamResult(byteOutputStream);
		
		try {
			Transformer transformer = _transFactory.newTransformer();
			transformer.transform(xmlSource, result);
			return new ByteArrayInputStream(byteOutputStream.toByteArray());
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
