package adis.helpers;

import java.util.HashMap;
import java.util.Map;

public class QueryStatements {
	private static final Map<String, String> _namedInserts = new HashMap<String, String>();
	private static final Map<String, String> _inserts = new HashMap<String, String>();
	
	static {
		_namedInserts.put("t_l_mpd_dac1_account_information", "INSERT INTO test.t_l_mpd_dac1_account_information VALUES (:gen_id, :bic, :iban, :oban, :isin, :osin, :other_account_info, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_address", "INSERT INTO test.t_l_mpd_dac1_address VALUES (:gen_id, :country, :address_free, :street, :building_identifier, :suite_identifier, :floor_identifier, :district_name, :pob, :post_code, :city, :country_sub_entity, :other_local_id, :address_type, :psc_norm, :psc_overeni, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_amount", "INSERT INTO test.t_l_mpd_dac1_amount VALUES (:gen_id, :id_kurz, :amount, :amount_jed, :stav_prep, :currency, :dc_prep, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_chyby_ref", "INSERT INTO test.t_l_mpd_dac1_chyby_ref VALUES (:gen_id, :id_message, :kod, :doc_ref_id, :corr_doc_ref_id_nenalezeny, :doc_ref_id_puvodni, :message_id_puvodni, :kategorie, :rok, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_financial_info", "INSERT INTO test.t_l_mpd_dac1_financial_info VALUES (:gen_id, :id_amount, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_generation_identifier", "INSERT INTO test.t_l_mpd_dac1_generation_identifier VALUES (:gen_id, :id_natural_person_name, :generation_identifier, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_kurz", "INSERT INTO test.t_l_mpd_dac1_kurz VALUES (:gen_id, :k_meny, :rok, :kurz, :mnozstvi, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_message", "INSERT INTO test.t_l_mpd_dac1_message VALUES (:gen_id, :rok, :kategorie, :originating_country, :destination_country, :message_id, :message_date, :message_type_indic, :tax_year, :c_zpravy_pz, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_middle_name", "INSERT INTO test.t_l_mpd_dac1_middle_name VALUES (:gen_id, :id_natural_person_name, :middle_name, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_name_suffix", "INSERT INTO test.t_l_mpd_dac1_name_suffix VALUES (:gen_id, :id_natural_person_name, :name_suffix, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_name_title", "INSERT INTO test.t_l_mpd_dac1_name_title VALUES (:gen_id, :id_natural_person_name, :title, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_natural_person", "INSERT INTO test.t_l_mpd_dac1_natural_person VALUES (:gen_id, :id_person, :acting_capacity, :gender, :country, :birth_date, :birth_city, :birth_country_subentity, :birth_country, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_natural_person_name", "INSERT INTO test.t_l_mpd_dac1_natural_person_name VALUES (:gen_id, :id_natural_person, :name_type, :name_free, :preceding_title, :first_name, :name_prefix, :last_name, :general_suffix, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_natural_person_nationality", "INSERT INTO test.t_l_mpd_dac1_natural_person_nationality VALUES (:gen_id, :id_natural_person, :country, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_natural_person_other_info", "INSERT INTO test.t_l_mpd_dac1_natural_person_other_info VALUES (:gen_id, :id_natural_person, :other_party_info, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_nnp_other_info", "INSERT INTO test.t_l_mpd_dac1_nnp_other_info VALUES (:gen_id, :id_non_natural_person, :other_party_info, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_non_natural_person", "INSERT INTO test.t_l_mpd_dac1_non_natural_person VALUES (:gen_id, :id_person, :non_natural_person_type, :non_natural_person_form, :country, :permanent_establishment, :commencement_date, :commencement_city, :commencement_country_subentity, :commencement_country, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_non_natural_person_name", "INSERT INTO test.t_l_mpd_dac1_non_natural_person_name VALUES (:gen_id, :id_non_natural_person, :nnp_name, :name_type, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_other_name_identifier", "INSERT INTO test.t_l_mpd_dac1_other_name_identifier VALUES (:gen_id, :id_natural_person_name, :other_name_identifier, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_payment", "INSERT INTO test.t_l_mpd_dac1_payment VALUES (:gen_id, :id_financial_info, :id_amount, :id_account_information, :payment_date, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_pen_administrator", "INSERT INTO test.t_l_mpd_dac1_pen_administrator VALUES (:gen_id, :id_person, :id_person_representative, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_pen_event", "INSERT INTO test.t_l_mpd_dac1_pen_event VALUES (:gen_id, :id_pen_scheme, :id_person_counterpart, :event_type, :event_start_date, :event_end_date, :periodicity, :reporting_type, :transfer_type, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_pen_event_info", "INSERT INTO test.t_l_mpd_dac1_pen_event_info VALUES (:gen_id, :id_pen_event, :id_financial_info, :existence, :taxation_type_event, :event_basis_type, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_pen_other_scheme_info", "INSERT INTO test.t_l_mpd_dac1_pen_other_scheme_info VALUES (:gen_id, :id_pen_scheme, :other_scheme_info, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_pen_payer", "INSERT INTO test.t_l_mpd_dac1_pen_payer VALUES (:gen_id, :id_person, :id_person_representative, :former_employer, :payer_type, :insurance_company, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_pen_recipient", "INSERT INTO test.t_l_mpd_dac1_pen_recipient VALUES (:gen_id, :id_person, :id_person_representative, :id_message, :stav_zaznamu, :nejasny_stav, :id_retezce, :recipient_number, :corr_doc_ref_id, :doc_ref_id, :doc_type_indic, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_pen_scheme", "INSERT INTO test.t_l_mpd_dac1_pen_scheme VALUES (:gen_id, :id_pen_recipient, :id_pen_payer, :id_pen_administrator, :scheme_nature, :scheme_taxation_right_source, :foreign_approved_scheme, :scheme_type, :tax_treatment_contributions, :scheme_kind, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_pen_scheme_capital_value", "INSERT INTO test.t_l_mpd_dac1_pen_scheme_capital_value VALUES (:gen_id, :id_pen_scheme, :id_amount, :capital_value_type, :capital_value_date, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_pen_scheme_owner", "INSERT INTO test.t_l_mpd_dac1_pen_scheme_owner VALUES (:gen_id, :id_person, :id_person_representative, :id_pen_scheme, :relationship_beneficiary, :scheme_owner_type, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_pen_scheme_ref_info", "INSERT INTO test.t_l_mpd_dac1_pen_scheme_ref_info VALUES (:gen_id, :id_pen_scheme, :scheme_reference_type, :scheme_reference, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_pen_special_activity_category", "INSERT INTO test.t_l_mpd_dac1_pen_special_activity_category VALUES (:gen_id, :id_pen_scheme, :special_activity_category, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_pen_tax_info", "INSERT INTO test.t_l_mpd_dac1_pen_tax_info VALUES (:gen_id, :id_pen_event, :id_financial_info, :tax, :existence, :taxation_type_tax, :taxable_basis_type, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_person", "INSERT INTO test.t_l_mpd_dac1_person VALUES (:gen_id, :typ, :first_name, :last_name, :middle_name, :name_title, :preceding_title, :birth_date, :birth_country, :birth_city, :name_free, :nnp_name, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_person_address", "INSERT INTO test.t_l_mpd_dac1_person_address VALUES (:gen_id, :id_person, :id_address, :hlavni, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_person_identifier", "INSERT INTO test.t_l_mpd_dac1_person_identifier VALUES (:gen_id, :id_person, :person_identifier_reference, :person_identifier_type, :country, :hlavni, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_ztotozneni", "INSERT INTO test.t_l_mpd_dac1_ztotozneni VALUES (:gen_id, :id_ztotozneni, :id_person, :id_zaznamu, :kategorie, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_lip_beneficiary", "INSERT INTO test.t_l_mpd_dac1_lip_beneficiary VALUES (:gen_id, :id_lip_policy, :id_lip_named_beneficiary, :id_lip_class_beneficiary, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_lip_benefit_duration", "INSERT INTO test.t_l_mpd_dac1_lip_benefit_duration VALUES (:gen_id, :benefit_type, :benefit_minimum_payable_years, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_lip_capital_value", "INSERT INTO test.t_l_mpd_dac1_lip_capital_value VALUES (:gen_id, :id_lip_policy, :id_amount, :capital_value_type, :capital_value_date, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_lip_class_beneficiary", "INSERT INTO test.t_l_mpd_dac1_lip_class_beneficiary VALUES (:gen_id, :id_person_representative, :class_identification, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_lip_class_life_insured", "INSERT INTO test.t_l_mpd_dac1_lip_class_life_insured VALUES (:gen_id, :id_person_representative, :class_identification, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_lip_contrib_duration", "INSERT INTO test.t_l_mpd_dac1_lip_contrib_duration VALUES (:gen_id, :contribution_type, :contrib_min_payable_years, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_lip_event", "INSERT INTO test.t_l_mpd_dac1_lip_event VALUES (:gen_id, :id_lip_policy, :id_person_counterpart, :event_actual_date, :event_deemed_date, :event_type, :event_scope, :transfer_type, :periodicity, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_lip_event_info", "INSERT INTO test.t_l_mpd_dac1_lip_event_info VALUES (:gen_id, :id_lip_event, :id_financial_info, :existence, :status, :taxation_type_event, :event_basis_type, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_lip_insurer", "INSERT INTO test.t_l_mpd_dac1_lip_insurer VALUES (:gen_id, :id_person, :id_person_representative, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_lip_life_insured", "INSERT INTO test.t_l_mpd_dac1_lip_life_insured VALUES (:gen_id, :id_lip_policy, :id_lip_named_life_insured, :id_lip_class_life_insured, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_lip_named_beneficiary", "INSERT INTO test.t_l_mpd_dac1_lip_named_beneficiary VALUES (:gen_id, :id_person, :id_person_representative, :beneficiary_type, :beneficiary_status, :beneficiary_share_type, :beneficiary_share_part, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_lip_named_life_insured", "INSERT INTO test.t_l_mpd_dac1_lip_named_life_insured VALUES (:gen_id, :id_person, :id_person_representative, :relationship_beneficiary, :life_insured_share_type, :life_insured_share_part, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_lip_payer_premium", "INSERT INTO test.t_l_mpd_dac1_lip_payer_premium VALUES (:gen_id, :id_lip_policy, :id_person, :id_person_representative, :relationship_beneficiary, :payer_type, :payer_share_type, :payer_share_part, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_lip_policy", "INSERT INTO test.t_l_mpd_dac1_lip_policy VALUES (:gen_id, :id_message, :id_lip_insurer, :id_lip_benefit_duration, :id_lip_contrib_duration, :id_lip_policy_option, :stav_zaznamu, :nejasny_stav, :id_retezce, :doc_ref_id, :corr_doc_ref_id, :doc_type_indic, :information_status, :policy_number, :eusd_indicator, :contribution_tax_treatement, :contribution_frequency, :policy_start_date, :policy_end_date, :profit_sharing, :policy_type, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_lip_policy_option", "INSERT INTO test.t_l_mpd_dac1_lip_policy_option VALUES (:gen_id, :early_redemption, :collateral_option, :transfer_option, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_lip_policy_other_info", "INSERT INTO test.t_l_mpd_dac1_lip_policy_other_info VALUES (:gen_id, :id_lip_policy, :policy_other_info, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_lip_policy_owner", "INSERT INTO test.t_l_mpd_dac1_lip_policy_owner VALUES (:gen_id, :id_lip_policy, :id_person, :id_person_representative, :relationship_beneficiary, :policy_owner_type, :policy_owner_share_type, :policy_owner_share_part, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_lip_tax_info", "INSERT INTO test.t_l_mpd_dac1_lip_tax_info VALUES (:gen_id, :id_lip_event, :id_financial_info, :tax, :existence, :taxation_type_tax, :taxable_basis_type, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_financial_info", "INSERT INTO test.t_l_mpd_dac1_financial_info VALUES (:gen_id, :id_amount, :dc_vloz, :dc_aktual)");
		_namedInserts.put("t_l_mpd_dac1_person", "INSERT INTO test.t_l_mpd_dac1_person VALUES (:gen_id, :typ, :first_name, :last_name, :middle_name, :name_title, :preceding_title, :birth_date, :birth_country, :birth_city, :name_free, :nnp_name, :first_name_norm, :last_name_norm, :middle_name_norm, :nnp_name_norm, :dc_vloz, :dc_aktual)");
		
		_inserts.put("t_l_mpd_dac1_account_information", "INSERT INTO test.t_l_mpd_dac1_account_information VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_address", "INSERT INTO test.t_l_mpd_dac1_address VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_amount", "INSERT INTO test.t_l_mpd_dac1_amount VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_chyby_ref", "INSERT INTO test.t_l_mpd_dac1_chyby_ref VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_financial_info", "INSERT INTO test.t_l_mpd_dac1_financial_info VALUES (?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_generation_identifier", "INSERT INTO test.t_l_mpd_dac1_generation_identifier VALUES (?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_kurz", "INSERT INTO test.t_l_mpd_dac1_kurz VALUES (?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_message", "INSERT INTO test.t_l_mpd_dac1_message VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_middle_name", "INSERT INTO test.t_l_mpd_dac1_middle_name VALUES (?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_name_suffix", "INSERT INTO test.t_l_mpd_dac1_name_suffix VALUES (?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_name_title", "INSERT INTO test.t_l_mpd_dac1_name_title VALUES (?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_natural_person", "INSERT INTO test.t_l_mpd_dac1_natural_person VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_natural_person_name", "INSERT INTO test.t_l_mpd_dac1_natural_person_name VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_natural_person_nationality", "INSERT INTO test.t_l_mpd_dac1_natural_person_nationality VALUES (?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_natural_person_other_info", "INSERT INTO test.t_l_mpd_dac1_natural_person_other_info VALUES (?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_nnp_other_info", "INSERT INTO test.t_l_mpd_dac1_nnp_other_info VALUES (?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_non_natural_person", "INSERT INTO test.t_l_mpd_dac1_non_natural_person VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_non_natural_person_name", "INSERT INTO test.t_l_mpd_dac1_non_natural_person_name VALUES (?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_other_name_identifier", "INSERT INTO test.t_l_mpd_dac1_other_name_identifier VALUES (?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_payment", "INSERT INTO test.t_l_mpd_dac1_payment VALUES (?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_pen_administrator", "INSERT INTO test.t_l_mpd_dac1_pen_administrator VALUES (?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_pen_event", "INSERT INTO test.t_l_mpd_dac1_pen_event VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_pen_event_info", "INSERT INTO test.t_l_mpd_dac1_pen_event_info VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_pen_other_scheme_info", "INSERT INTO test.t_l_mpd_dac1_pen_other_scheme_info VALUES (?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_pen_payer", "INSERT INTO test.t_l_mpd_dac1_pen_payer VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_pen_recipient", "INSERT INTO test.t_l_mpd_dac1_pen_recipient VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_pen_scheme", "INSERT INTO test.t_l_mpd_dac1_pen_scheme VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_pen_scheme_capital_value", "INSERT INTO test.t_l_mpd_dac1_pen_scheme_capital_value VALUES (?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_pen_scheme_owner", "INSERT INTO test.t_l_mpd_dac1_pen_scheme_owner VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_pen_scheme_ref_info", "INSERT INTO test.t_l_mpd_dac1_pen_scheme_ref_info VALUES (?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_pen_special_activity_category", "INSERT INTO test.t_l_mpd_dac1_pen_special_activity_category VALUES (?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_pen_tax_info", "INSERT INTO test.t_l_mpd_dac1_pen_tax_info VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_person", "INSERT INTO test.t_l_mpd_dac1_person VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_person_address", "INSERT INTO test.t_l_mpd_dac1_person_address VALUES (?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_person_identifier", "INSERT INTO test.t_l_mpd_dac1_person_identifier VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_ztotozneni", "INSERT INTO test.t_l_mpd_dac1_ztotozneni VALUES (?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_lip_beneficiary", "INSERT INTO test.t_l_mpd_dac1_lip_beneficiary VALUES (?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_lip_benefit_duration", "INSERT INTO test.t_l_mpd_dac1_lip_benefit_duration VALUES (?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_lip_capital_value", "INSERT INTO test.t_l_mpd_dac1_lip_capital_value VALUES (?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_lip_class_beneficiary", "INSERT INTO test.t_l_mpd_dac1_lip_class_beneficiary VALUES (?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_lip_class_life_insured", "INSERT INTO test.t_l_mpd_dac1_lip_class_life_insured VALUES (?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_lip_contrib_duration", "INSERT INTO test.t_l_mpd_dac1_lip_contrib_duration VALUES (?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_lip_event", "INSERT INTO test.t_l_mpd_dac1_lip_event VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_lip_event_info", "INSERT INTO test.t_l_mpd_dac1_lip_event_info VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_lip_insurer", "INSERT INTO test.t_l_mpd_dac1_lip_insurer VALUES (?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_lip_life_insured", "INSERT INTO test.t_l_mpd_dac1_lip_life_insured VALUES (?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_lip_named_beneficiary", "INSERT INTO test.t_l_mpd_dac1_lip_named_beneficiary VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_lip_named_life_insured", "INSERT INTO test.t_l_mpd_dac1_lip_named_life_insured VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_lip_payer_premium", "INSERT INTO test.t_l_mpd_dac1_lip_payer_premium VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_lip_policy", "INSERT INTO test.t_l_mpd_dac1_lip_policy VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_lip_policy_option", "INSERT INTO test.t_l_mpd_dac1_lip_policy_option VALUES (?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_lip_policy_other_info", "INSERT INTO test.t_l_mpd_dac1_lip_policy_other_info VALUES (?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_lip_policy_owner", "INSERT INTO test.t_l_mpd_dac1_lip_policy_owner VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_lip_tax_info", "INSERT INTO test.t_l_mpd_dac1_lip_tax_info VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_financial_info", "INSERT INTO test.t_l_mpd_dac1_financial_info VALUES (?, ?, ?, ?)");
		_inserts.put("t_l_mpd_dac1_person", "INSERT INTO test.t_l_mpd_dac1_person VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	}
	
	public static String getNamedInsertQuery(String table) {
		return _namedInserts.get(table);
	}
	
	public static String getInsertQuery(String table) {
		return _inserts.get(table);
	}
}
