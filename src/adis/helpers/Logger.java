package adis.helpers;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
	private boolean DEBUG = false;
	
	public Logger() {
		init();
	}
	
	private void init() {
		this.DEBUG = true;
	}
	
	public void log(String text) {
		if(this.DEBUG) {
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			String caller = String.format("%s", getCallerName());
			caller = caller.replaceAll("^(.*?)+\\.", "");
			
			System.out.format("[%s] %s: %s\n", formatter.format(date), caller, text);
		}
	}
	
	private String getCallerName() {
		StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
		
        for (int i = 1; i < stElements.length; i++) {
            StackTraceElement element = stElements[i];
            if (!element.getClassName().equals(Logger.class.getName()) && element.getClassName().indexOf("java.lang.Thread") != 0) {
                return element.getClassName().replaceAll(".+\\.", "");
            }
        }
        return null;
	}
}
