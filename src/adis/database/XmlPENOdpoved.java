package adis.database;

import java.sql.SQLException;
import java.sql.Date;
import java.text.ParseException;
import java.time.LocalDateTime;

import _int.eu.cec.taxud.directtax.v3.DirectTaxMessage;
import _int.eu.cec.taxud.directtax.v3.HeaderType;

import adis.factories.TaxMessageFactory;
import adis.helpers.NamedStatement;
import adis.helpers.QueryStatements;
import adis.interfaces.IXmlOdpoved;
import eu.europa.ec.taxud.directtax.aeoi.v1.PENPayerType;
import adis.factories.PENPayerFactory;
import adis.factories.PENRecipientFactory;
import adis.factories.PENSchemeEventFactory;

public class XmlPENOdpoved extends XmlOdpoved {
	public XmlPENOdpoved(DirectTaxMessage directTaxMessage, DbConnection connection) {
		super(directTaxMessage, connection);
		
		this.setTaxMessageFactory(new TaxMessageFactory(directTaxMessage));
		this.setRecipientFactory(getTaxMessageFactory().getRecipientNaturalPerson());
		this.setPayerFactory(getTaxMessageFactory().getPayer());
		this.setSchemeEventFactory(getTaxMessageFactory().getSchemeEvent());
	}
	
	@Override
	public void process() throws SQLException, ParseException {
		getDbConnection().connect();
		getDbConnection().setAutoCommit(false);
		
		DbRecordTester dbTester = new DbRecordTester(getDbConnection());
		HeaderType header = getTaxMessageFactory().getHeader();
		DbQuery query = new DbQuery(getDbConnection());
		NamedStatement statement;
		
		TaxMessageFactory taxMessage = getTaxMessageFactory();
		PENRecipientFactory recipient = getRecipientFactory();
		PENPayerFactory payer = getPayerFactory();
		PENSchemeEventFactory schemeEvent = getSchemeEventFactory();
		
		for(PENPayerType payerType : payer.getPayers()) {
				
		}
		
		int messageId = this.getNewId("t_l_mpd_dac1_message");
		int recipientId = this.getNewId("t_l_mpd_dac1_pen_recipient");
		int payerId = this.getNewId("t_l_mpd_dac1_pen_payer");
		int schemeId = this.getNewId("t_l_mpd_dac1_pen_scheme");
		int specialActivityCategoryId = this.getNewId("t_l_mpd_dac1_pen_special_activity_category");
		int otherSchemeInfoId = this.getNewId("t_l_mpd_dac1_pen_other_scheme_info");
		int schemeRefInfoId = this.getNewId("t_l_mpd_dac1_pen_scheme_ref_info");
		int schemeCapitalValueId = this.getNewId("t_l_mpd_dac1_pen_scheme_capital_value");
		int administratorId = this.getNewId("t_l_mpd_dac1_pen_administrator");
		int schemeOwnerId = this.getNewId("t_l_mpd_dac1_pen_scheme_owner");
		int eventId = this.getNewId("t_l_mpd_dac1_pen_event");
		int taxInfoId = this.getNewId("t_l_mpd_dac1_pen_tax_info");
		int eventInfoId = this.getNewId("t_l_mpd_dac1_pen_event_info");
		
		
		if(dbTester.recordExists("test.t_l_mpd_dac1_message", "gen_id = " + messageId)) {
			this.getDbConnection().delete(String.format(DbQuery.DELETE, "test.t_l_mpd_dac1_message", "gen_id = " + messageId));
			this.getDbConnection().commit();
		}
		
		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_message"));
		statement.setValue("gen_id", messageId);
		statement.setValue("rok", taxMessage.getBodyTaxYear().getYear());
		statement.setValue("kategorie", "PEN");
		statement.setValue("originating_country", header.getOriginatingCountry());
		statement.setValue("destination_country", header.getDestinationCountries().get(0));
		statement.setValue("message_id", header.getMessageId());
		statement.setValue("message_date", header.getTimestamp().toString().replace("T", " "));
		statement.setValue("message_type_indic", header.getMessageTypeIndic());
		statement.setValue("tax_year", Date.valueOf(getTaxMessageFactory().getBodyTaxYear().toString()));
		statement.setValue("c_zpravy_pz", 0);
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());
		
		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_pen_recipient"));
		statement.setValue("gen_id", recipientId);
		statement.setValue("id_person", 0);
		statement.setValue("id_person_representative", this.getPayerFactory().getNonNaturalRepresentative().getID().get(0).getRef());
		statement.setValue("id_message", messageId);
		statement.setValue("stav_zaznamu", 0);
		statement.setValue("nejasny_stav", 0);
		statement.setValue("id_retezce", 0);
		statement.setValue("recipient_number", recipient.getRecipientNumber());
		statement.setValue("corr_doc_ref_id", taxMessage.getDocSpec().getCorrDocRefId());
		statement.setValue("doc_ref_id", taxMessage.getDocSpec().getDocRefId());
		statement.setValue("doc_type_indic", taxMessage.getDocSpec().getDocTypeIndic());
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());
		
		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_pen_payer"));
		statement.setValue("gen_id", payerId);
		statement.setValue("id_person", payer.getNonNaturalPerson().getID().get(0).getRef());
		statement.setValue("id_person_representative", payer.getNonNaturalRepresentative().getID().get(0).getRef());
		statement.setValue("former_employer", payer.isFormerEmployer());
		statement.setValue("payer_type", payer.getPayerTypeType().value());
		statement.setValue("insurance_company", payer.isInsuranceCompany());
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());
		
		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_pen_scheme"));
		statement.setValue("gen_id", schemeId);
		statement.setValue("id_pen_recipient", recipientId);
		statement.setValue("id_pen_payer", payerId);
		statement.setValue("id_pen_administrator", administratorId);
		statement.setValue("scheme_nature", payer.getScheme().getSchemeNature().value());
		statement.setValue("scheme_taxation_right_source", payer.getScheme().getTaxationRightSource().value());
		statement.setValue("foreign_approved_scheme", payer.getScheme().isForeignApprovedScheme().booleanValue());
		statement.setValue("scheme_type", payer.getScheme().getSchemeType().value());
		statement.setValue("tax_treatment_contributions", payer.getScheme().getTaxTreatmentContributions().value());
		statement.setValue("scheme_kind", payer.getScheme().getSchemeKind().value());
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());

		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_pen_special_activity_category"));
		statement.setValue("gen_id", specialActivityCategoryId);
		statement.setValue("id_pen_scheme", schemeId);
		statement.setValue("special_activity_category", payer.getScheme().getSpecialActivityCategories().getSpecialActivityCategory().get(0).value());
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());
		
		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_pen_other_scheme_info"));
		statement.setValue("gen_id", otherSchemeInfoId);
		statement.setValue("id_pen_scheme", schemeId);
		statement.setValue("other_scheme_info", payer.getScheme().getOtherInfo().get(0));
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());
		
		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_pen_scheme_ref_info"));
		statement.setValue("gen_id", schemeRefInfoId);
		statement.setValue("id_pen_scheme", schemeId);
		statement.setValue("scheme_reference_type", payer.getScheme().getReferenceInfos().getReferenceInfo().get(0).getSchemeReferenceType().value());
		statement.setValue("scheme_reference", payer.getScheme().getReferenceInfos().getReferenceInfo().get(0).getSchemeReference());
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());
		
		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_pen_scheme_capital_value"));
		statement.setValue("gen_id", schemeCapitalValueId);
		statement.setValue("id_pen_scheme", schemeId);
		statement.setValue("id_amount", payer.getScheme().getCapitalValues().getCapitalValue().get(0).getAmount().getValue());
		statement.setValue("capital_value_type", payer.getScheme().getCapitalValues().getCapitalValue().get(0).getSchemeCapitalValueType().value());
		statement.setValue("capital_value_date", payer.getScheme().getCapitalValues().getCapitalValue().get(0).getDate().getDateYMD());
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());
		
		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_pen_administrator"));
		statement.setValue("gen_id", administratorId);
		statement.setValue("id_person", 0);
		statement.setValue("id_person_representative", 0);
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());
		
		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_pen_scheme_owner"));
		statement.setValue("gen_id", schemeOwnerId);
		statement.setValue("id_person", 0);
		statement.setValue("id_person_representative", 0);
		statement.setValue("id_pen_scheme", schemeId);
		statement.setValue("relationship_beneficiary", payer.getScheme().getOwners().getOwner().get(0).getRelationshipToRecipient());
		statement.setValue("scheme_owner_type", payer.getScheme().getOwners().getOwner().get(0).getSchemeOwnerType());
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());
		
		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_pen_event"));
		statement.setValue("gen_id", eventId);
		statement.setValue("id_pen_scheme", schemeId);
		statement.setValue("id_person_counterpart", 0);
		statement.setValue("event_type", schemeEvent.getEventType());
		statement.setValue("event_start_date", schemeEvent.getStartDate());
		statement.setValue("event_end_date", schemeEvent.getEndDate());
		statement.setValue("periodicity", schemeEvent.getPeriodicity());
		statement.setValue("reporting_type", schemeEvent.getReportingType());
		statement.setValue("transfer_type", schemeEvent.getTransferType());
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());
		
		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_pen_tax_info"));
		statement.setValue("gen_id", taxInfoId);
		statement.setValue("id_pen_event", eventId);
		statement.setValue("id_financial_info", 0);
		statement.setValue("tax", schemeEvent.getTax());
		statement.setValue("existence", schemeEvent.isEventExistence());
		statement.setValue("taxation_type_tax", schemeEvent.getTaxationTypeTax());
		statement.setValue("taxable_basis_type", schemeEvent.getTaxableBasisType());
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());
		
		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_pen_event_info"));
		statement.setValue("gen_id", eventInfoId);
		statement.setValue("id_pen_event", eventId);
		statement.setValue("id_financial_info", 0);
		statement.setValue("existence", schemeEvent.getEventInfoType().isExistence());
		statement.setValue("taxation_type_event", schemeEvent.getEventInfoType().getTaxationTypeEvent().value());
		statement.setValue("event_basis_type", schemeEvent.getEventBasisType());
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());
		
		//query.execute();
		getDbConnection().close();
	}
}