package adis.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import adis.interfaces.IDbRecordTester;

public class DbRecordTester implements IDbRecordTester {
	private DbConnection _dbConnection;
	private final String _sqlSelect = "SELECT COUNT(*) FROM %s WHERE %s";
	
	public DbRecordTester(DbConnection connection) {
		this._dbConnection = connection;
	}
	
	/**
	 * Tests the existence of a record in a DB.
	 * Example: SELECT FROM [table] WHERE [whereClause]
	 * @param table The name of the table.
	 * @param whereClause The SELECT condition.
	 * @return boolean If record exists or not.
	 * @throws SQLException
	 * */
	public boolean recordExists(String table, String whereClause) throws SQLException {
		Statement statement = getDbConnection().getConnection().createStatement();
		ResultSet result = statement.executeQuery(String.format(this._sqlSelect, table, whereClause));
		
		while(result.next()) {
			int tmp = result.getInt(1);
			if(tmp > 0) {
				return true;
			}
		}
		return false;
	}
	
	private DbConnection getDbConnection() {
		return this._dbConnection;
	}
}
