package adis.database;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;


import adis.helpers.DbConfigParser;
import adis.helpers.Logger;
import adis.interfaces.IDbConnection;

public class DbConnection implements IDbConnection {
	private Connection _connection = null;
	private String _connectionString = "";
	private Logger _logger = new Logger();
	
	public DbConnection() {
		try {
			Class.forName("com.informix.jdbc.IfxDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		HashMap<String, String> config = getConfigHashMap();
		
		this._connectionString = "jdbc:informix-sqli://" 
				+ config.get("db_host") + ":" 
				+ config.get("db_port") + "/" 
				+ config.get("db_name") + 
				":informixserver=" + config.get("db_server") + 
				";user=" + config.get("db_username") + 
				";password=" + config.get("db_password");
	}
	
	private HashMap<String, String> getConfigHashMap() {
		try {
			DbConfigParser configParser = new DbConfigParser();
			return configParser.getConfigMap();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void commit() throws SQLException {
		this.getConnection().commit();
	}
	
	public Connection getConnection() {
		return this._connection;
	}
	
	public void setConnection(Connection connection) {
		this._connection = connection;
	}
	
	public void update(String updateCommand) throws SQLException {
		Statement statement = getConnection().createStatement();
		statement.executeUpdate(updateCommand);
	}
	
	public void insert(String insertCommand) throws SQLException {
			Statement statement = getConnection().createStatement();
			statement.execute(insertCommand);
	}
	
	public void delete(String deleteCommand) throws SQLException {
			Statement statement = getConnection().createStatement();
			statement.execute(deleteCommand);
	}

	public void connect() {
		try {
			this._logger.log("Establishing DB connection...");
			this._connection = DriverManager.getConnection(this._connectionString);
		} catch(SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	public void close() {
		try {
			this._logger.log("Closing DB connection...");
			this._connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Checks if the connection has valid information to be able to connect to the DB
	 * @return boolean if it successfully connected to the DB
	 * */
	public boolean isValid() {
		try {
			return this._connection.isValid(5);
		} catch (SQLException | NullPointerException e) {
			return false;
		}
	}
	
	public PreparedStatement getPreparedStatement(String sql) throws SQLException {
		return this.getConnection().prepareStatement(sql);
	}
	
	public Statement getStatement() throws SQLException {
		return this.getConnection().createStatement();
	}
	
	public DatabaseMetaData getMetaData() throws SQLException {
		return this.getConnection().getMetaData();
	}
	
	public boolean getAutoCommit() throws SQLException {
		return this.getConnection().getAutoCommit();
	}
	
	public void setAutoCommit(Boolean value) throws SQLException {
		this.getConnection().setAutoCommit(value);
	}
 }