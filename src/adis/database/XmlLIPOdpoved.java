package adis.database;

import java.sql.SQLException;
import java.time.LocalDateTime;

import _int.eu.cec.taxud.directtax.v3.DirectTaxMessage;
import adis.factories.LIPBeneficiaryFactory;
import adis.factories.LIPEventFactory;
import adis.factories.LIPPolicyFactory;
import adis.factories.TaxMessageFactory;
import adis.helpers.NamedStatement;
import adis.helpers.QueryStatements;
import eu.europa.ec.taxud.directtax.aeoi.v1.AEOILIPBodyType;

public class XmlLIPOdpoved extends XmlOdpoved {
	private AEOILIPBodyType _body;
	
	public XmlLIPOdpoved(DirectTaxMessage directTaxMessage, DbConnection connection) {
		super(directTaxMessage, connection);
		
		this.setTaxMessageFactory(new TaxMessageFactory(directTaxMessage));
		this.setPolicyFactory(getTaxMessageFactory().getPolicyFactory());
		this.setBeneficiaryFactory(getTaxMessageFactory().getBeneficiaryFactory());
	}
	
	@Override
	public void process() throws SQLException {
		this.getDbConnection().connect();
		this.getDbConnection().setAutoCommit(false);
		
		NamedStatement statement;
		DbQuery query = new DbQuery(this.getDbConnection());
		
		LIPEventFactory event = new LIPEventFactory(getPolicyFactory().getEvents());
		LIPPolicyFactory policy = getPolicyFactory();
		LIPBeneficiaryFactory beneficiary = getBeneficiaryFactory();
		
		int beneficiaryId = this.getNewId("t_l_mpd_dac1_lip_beneficiary");
		int policyId = this.getNewId("t_l_mpd_dac1_lip_policy");
		int namedBeneficiaryId = this.getNewId("t_l_mpd_dac1_lip_named_beneficiary");
		int benefitDurationId = this.getNewId("t_l_mpd_dac1_lip_benefit_duration");
		int capitalValueId = this.getNewId("t_l_mpd_dac1_lip_capital_value");
		int classBeneficiaryId = this.getNewId("t_l_mpd_dac1_lip_class_beneficiary");
		int classLifeInsuredId = this.getNewId("t_l_mpd_dac1_lip_class_life_insured");
		int namedLifeInsuredId = this.getNewId("t_l_mpd_dac1_lip_named_life_insured");
		int contribDurationId = this.getNewId("t_l_mpd_dac1_lip_contrib_duration");
		int eventId = this.getNewId("t_l_mpd_dac1_lip_event");
		int eventInfoId = this.getNewId("t_l_mpd_dac1_lip_event_info");
		int lifeInsuredId = this.getNewId("t_l_mpd_dac1_lip_life_insured");
		int payerPremiumId = this.getNewId("t_l_mpd_dac1_lip_payer_premium");
		int messageId = this.getNewId("t_l_mpd_dac1_message");
		int insurerId = this.getNewId("t_l_mpd_dac1_lip_insurer");
		int policyOptionId = this.getNewId("t_l_mpd_dac1_lip_policy_option");
		int taxInfoId = this.getNewId("t_l_mpd_dac1_lip_tax_info");
		int policyOwnerId = this.getNewId("t_l_mpd_dac1_lip_policy_owner");
		int policyOtherInfoId = this.getNewId("t_l_mpd_dac1_lip_policy_other_info");
		
		System.out.println(this.getBeneficiaryFactory().getNamedBeneficiary().getBeneficiaryType());
		
		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_lip_beneficiary"));
		statement.setValue("gen_id", beneficiaryId);
		statement.setValue("id_lip_policy", policyId);
		statement.setValue("id_lip_named_beneficiary", benefitDurationId);
		statement.setValue("id_lip_class_beneficiary", classBeneficiaryId);
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());

		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_lip_benefit_duration"));
		statement.setValue("gen_id", benefitDurationId);
		statement.setValue("benefit_type", policy.getBeneficiaries().getNamedBeneficiary().getBeneficiaryType());
		statement.setValue("benefit_minimum_payable_years", 0);
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());

		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_lip_capital_value"));
		statement.setValue("gen_id", capitalValueId);
		statement.setValue("id_lip_policy", policyId);
		statement.setValue("id_amount", 0);
		statement.setValue("capital_value_type", 0);
		statement.setValue("capital_value_date", 0);
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());

		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_lip_class_beneficiary"));
		statement.setValue("gen_id", classBeneficiaryId);
		statement.setValue("id_person_representative", 0);
		statement.setValue("class_identification", 0);
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());

		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_lip_class_life_insured"));
		statement.setValue("gen_id", classLifeInsuredId);
		statement.setValue("id_person_representative", 0);
		statement.setValue("class_identification", 0);
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());

		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_lip_contrib_duration"));
		statement.setValue("gen_id", contribDurationId);
		statement.setValue("contribution_type", 0);
		statement.setValue("contrib_min_payable_years", 0);
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());

		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_lip_event"));
		statement.setValue("gen_id", eventId);
		statement.setValue("id_lip_policy", policyId);
		statement.setValue("id_person_counterpart", 0);
		statement.setValue("event_actual_date", 0);
		statement.setValue("event_deemed_date", 0);
		statement.setValue("event_type", 0);
		statement.setValue("event_scope", event.getScope());
		statement.setValue("transfer_type", event.getTransfer());
		statement.setValue("periodicity", event.getPeriodicity());
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());

		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_lip_event_info"));
		statement.setValue("gen_id", eventInfoId);
		statement.setValue("id_lip_event", eventId);
		statement.setValue("id_financial_info", 0);
		statement.setValue("existence", event.getEventInfo().isExistence());
		statement.setValue("status", event.getEventInfo().getStatus());
		statement.setValue("taxation_type_event", event.getEventInfo().getTaxationTypeEvent());
		statement.setValue("event_basis_type", event.getEventInfo().getEventBasisType());
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());

		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_lip_insurer"));
		statement.setValue("gen_id", insurerId);
		statement.setValue("id_person", 0);
		statement.setValue("id_person_representative", 0);
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());

		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_lip_life_insured"));
		statement.setValue("gen_id", lifeInsuredId);
		statement.setValue("id_lip_policy", policyId);
		statement.setValue("id_lip_named_life_insured", namedLifeInsuredId);
		statement.setValue("id_lip_class_life_insured", classLifeInsuredId);
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());

		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_lip_named_beneficiary"));
		statement.setValue("gen_id", namedBeneficiaryId);
		statement.setValue("id_person", 0);
		statement.setValue("id_person_representative", 0);
		statement.setValue("beneficiary_type", beneficiary.getNamedBeneficiary().getBeneficiaryType());
		statement.setValue("beneficiary_status", beneficiary.getNamedBeneficiary().getStatus());
		statement.setValue("beneficiary_share_type", beneficiary.getNamedBeneficiary().getShareType());
		statement.setValue("beneficiary_share_part", beneficiary.getNamedBeneficiary().getSharePart());
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());

		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_lip_named_life_insured"));
		statement.setValue("gen_id", namedLifeInsuredId);
		statement.setValue("id_person", 0);
		statement.setValue("id_person_representative", 0);
		statement.setValue("relationship_beneficiary", 0);
		statement.setValue("life_insured_share_type", 0);
		statement.setValue("life_insured_share_part", 0);
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());

		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_lip_payer_premium"));
		statement.setValue("gen_id", payerPremiumId);
		statement.setValue("id_lip_policy", policyId);
		statement.setValue("id_person", 0);
		statement.setValue("id_person_representative", 0);
		statement.setValue("relationship_beneficiary", 0);
		statement.setValue("payer_type", 0);
		statement.setValue("payer_share_type", 0);
		statement.setValue("payer_share_part", 0);
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());

		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_lip_policy"));
		statement.setValue("gen_id", policyId);
		statement.setValue("id_message", messageId);
		statement.setValue("id_lip_insurer", insurerId);
		statement.setValue("id_lip_benefit_duration", benefitDurationId);
		statement.setValue("id_lip_contrib_duration", contribDurationId);
		statement.setValue("id_lip_policy_option", policyOptionId);
		statement.setValue("stav_zaznamu", 0);
		statement.setValue("nejasny_stav", 0);
		statement.setValue("id_retezce", 0);
		statement.setValue("doc_ref_id", policy.getPolicyType().getDocSpec().getDocRefId());
		statement.setValue("corr_doc_ref_id", policy.getPolicyType().getDocSpec().getCorrDocRefId());
		statement.setValue("doc_type_indic", policy.getPolicyType().getDocSpec().getDocTypeIndic());
		statement.setValue("information_status", 0);
		statement.setValue("policy_number", policy.getPolicyType().getPolicyNumber());
		statement.setValue("eusd_indicator", policy.getPolicyType().isEUSDIndicator());
		statement.setValue("contribution_tax_treatement", policy.getPolicyType().getContributionTaxTreatment());
		statement.setValue("contribution_frequency", policy.getPolicyType().getContributionFrequency());
		statement.setValue("policy_start_date", policy.getStartDate());
		statement.setValue("policy_end_date", policy.getEndDate());
		statement.setValue("profit_sharing", policy.getPolicyType().isProfitSharing());
		statement.setValue("policy_type", policy.getPolicyType().getPolicyType());
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());

		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_lip_policy_option"));
		statement.setValue("gen_id", policyOptionId);
		statement.setValue("early_redemption", 0);
		statement.setValue("collateral_option", 0);
		statement.setValue("transfer_option", 0);
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());

		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_lip_policy_other_info"));
		statement.setValue("gen_id", policyOtherInfoId);
		statement.setValue("id_lip_policy", policyId);
		statement.setValue("policy_other_info", policy.getOtherInfo());
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());

		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_lip_policy_owner"));
		statement.setValue("gen_id", policyOwnerId);
		statement.setValue("id_lip_policy", policyId);
		statement.setValue("id_person", 0);
		statement.setValue("id_person_representative", 0);
		statement.setValue("relationship_beneficiary", 0);
		statement.setValue("policy_owner_type", 0);
		statement.setValue("policy_owner_share_type", 0);
		statement.setValue("policy_owner_share_part", 0);
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());

		statement = new NamedStatement(QueryStatements.getNamedInsertQuery("t_l_mpd_dac1_lip_tax_info"));
		statement.setValue("gen_id", taxInfoId);
		statement.setValue("id_lip_event", eventId);
		statement.setValue("id_financial_info", 0);
		statement.setValue("tax", event.getTaxType());
		statement.setValue("existence", event.getEventInfo().isExistence());
		statement.setValue("taxation_type_tax", 0);
		statement.setValue("taxable_basis_type", 0);
		statement.setValue("dc_vloz", LocalDateTime.now());
		statement.setValue("dc_aktual", LocalDateTime.now());
		query.addNamedStatement(statement.getSQL());
		
		getDbConnection().close();
		
		System.out.println(policy.getPolicyType().isEUSDIndicator());
	}
	
	public AEOILIPBodyType getBodyType() {
		return this._body;
	}
	
	public void setBodyType(AEOILIPBodyType body) {
		this._body = body;
	}
}
