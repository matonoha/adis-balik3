package adis.database;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DbQuery {
	public static String SELECT = "SELECT % FROM %s WHERE %s";
	public static String UPDATE = "UPDATE %s SET %s WHERE %s";
	public static String DELETE = "DELETE FROM %s WHERE %s";
	public static String INSERT = "INSERT INTO %s VALUES (%s)";
	
	private DbConnection _dbConnection;
	private List<PreparedStatement> _statements;
	private List<Statement> _namedStatements;
	private Statement _insertStatement;
	
	public DbQuery(DbConnection connection) throws SQLException {
		setStatements(new ArrayList<PreparedStatement>());
		setNamedStatements(new ArrayList<Statement>());
		
		setInsertStatement(connection.getStatement());
		setDbConnection(connection);
	}
	
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		return this.getDbConnection().getPreparedStatement(sql);
	}
	
	public void addPreparedStatement(PreparedStatement statement) {
		getStatements().add(statement);
	}
	
	public void addNamedStatement(String namedStatementQuery) throws SQLException {
		System.out.println(namedStatementQuery);
		getInsertStatement().addBatch(namedStatementQuery);
	}
	
	private Statement getInsertStatement() {
		return this._insertStatement;
	}
	 
	private void setInsertStatement(Statement value) {
		this._insertStatement = value;
	}
	
	private List<PreparedStatement> getStatements() {
		return this._statements;
	}
	
	private void setStatements(List<PreparedStatement> value) {
		this._statements = value;
	}
	
	private void setNamedStatements(List<Statement> value) {
		this._namedStatements = value;
	}
	
	private DbConnection getDbConnection() {
		return this._dbConnection;
	}
	
	public void execute() throws SQLException {
		for(PreparedStatement statement : _statements) {
			statement.execute();
		}
		
		getInsertStatement().executeBatch();
		
		getDbConnection().commit();
		getDbConnection().setAutoCommit(true);
		getStatements().clear();
	}
	
	private void setDbConnection(DbConnection value) {
		this._dbConnection = value;
	}
}
