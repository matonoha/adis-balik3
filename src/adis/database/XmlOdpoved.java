package adis.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;

import _int.eu.cec.taxud.directtax.v3.DirectTaxMessage;
import adis.factories.LIPBeneficiaryFactory;
import adis.factories.LIPPolicyFactory;
import adis.factories.PENPayerFactory;
import adis.factories.PENRecipientFactory;
import adis.factories.PENSchemeEventFactory;
import adis.factories.TaxMessageFactory;

public class XmlOdpoved {
	protected DbConnection _dbconnection;
	protected DirectTaxMessage _message;
	protected TaxMessageFactory _taxMessageFactory;
	protected PENRecipientFactory _recipientFactory;
	protected PENPayerFactory _payerFactory;
	protected PENSchemeEventFactory _schemeEventFactory;
	protected LIPBeneficiaryFactory _beneficiaryFactory;
	protected LIPPolicyFactory _policyFactory;
	
	public XmlOdpoved(DirectTaxMessage directTaxMessage, DbConnection dbConnection) {
		this._dbconnection = dbConnection;
		this._message = directTaxMessage;
	}
	
	public void process() throws SQLException, ParseException {	}
	
	protected DbConnection getDbConnection() {
		return this._dbconnection;
	}

	protected TaxMessageFactory getTaxMessageFactory() {
		return _taxMessageFactory;
	}

	protected PENRecipientFactory getRecipientFactory() {
		return _recipientFactory;
	}

	protected PENPayerFactory getPayerFactory() {
		return _payerFactory;
	}
	
	protected PENSchemeEventFactory getSchemeEventFactory() {
		return _schemeEventFactory;
	}

	protected void setTaxMessageFactory(TaxMessageFactory taxMessageFactory) {
		this._taxMessageFactory = taxMessageFactory;
	}

	protected void setRecipientFactory(PENRecipientFactory recipientFactory) {
		this._recipientFactory = recipientFactory;
	}

	protected void setPayerFactory(PENPayerFactory payerFactory) {
		this._payerFactory = payerFactory;
	}

	protected void setSchemeEventFactory(PENSchemeEventFactory schemeEventFactory) {
		this._schemeEventFactory = schemeEventFactory;
	}

	protected LIPBeneficiaryFactory getBeneficiaryFactory() {
		return _beneficiaryFactory;
	}

	protected LIPPolicyFactory getPolicyFactory() {
		return _policyFactory;
	}

	protected void setBeneficiaryFactory(LIPBeneficiaryFactory beneficiaryFactory) {
		this._beneficiaryFactory = beneficiaryFactory;
	}

	protected void setPolicyFactory(LIPPolicyFactory policyFactory) {
		this._policyFactory = policyFactory;
	}
	
	protected int getNewId(String tableName) throws SQLException {
		Statement statement = getDbConnection().getConnection().createStatement();
		ResultSet result = statement.executeQuery(String.format("SELECT COUNT(*) FROM %s", tableName));
		
		while(result.next()) {
			int tmp = result.getInt(1);
			if(tmp > 0) {
				return tmp;
			}
		}
		return 0;
	}
}
